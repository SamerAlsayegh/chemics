package kz.pvp.chemics.enums;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Formula {
	String originalFormula;
	static String modifiedFormula;
	Double[] doubleValues = new Double[0];
	String[] modifiedFormulas = new String[10];
	public Formula(String original, String[] variableValues) {
		originalFormula = original;
		modifiedFormula = original;



		// Format:     VarSign : VarValue
		for (String varInfo : variableValues){
			System.out.println( varInfo.split(":")[0] + " from " + varInfo);
			modifiedFormula = modifiedFormula.replaceAll(varInfo.split(":")[0], ""+Double.parseDouble(varInfo.split(":")[1]));
		}
		/*
		int usedVal = 0;
		doubleValues = values;
		for(int y = 0; y < z; y++){
			char character = modifiedFormula.charAt(y);
			if(Character.isUpperCase(character)){
				modifiedFormula = originalFormula.replaceAll(""+character, ""+values[usedVal]);
				usedVal++;
			}
		}
		 */
	}

	static String fnum = null;
	static String lnum = null;
	static String total = null;


	public String Math(String operationFunc) {
		String mathoperation = modifiedFormula;
		if (operationFunc != null)
			mathoperation = operationFunc;

		Pattern pattern = Pattern.compile("\\)");
		Matcher  matcher = pattern.matcher(mathoperation);

		int count = 0;
		while (matcher.find())
			count++;

		System.out.println("Found: " + count + " in " + mathoperation);
		if (count > 0){
			modifiedFormulas = new String[count+1];
			modifiedFormulas[0] = modifiedFormula;
			for (int x = 0; x < count; x++){
				String[] results = mathoperation.split("\\)")[0].split("\\(");
				int l = results.length;
				for (int j = 0; j < l / 2; j++) {
					String temp = results[j];
					results[j] = results[l - j - 1];
					results[l - j - 1] = temp;
				}
				modifiedFormulas[x+1] = mathoperation.replace("(" + results[0] + ")", "(" + Math(results[0]) + ")");
				mathoperation = mathoperation.replace("(" + results[0] + ")", Math(results[0]));
			}
		}

		mathoperation = mathoperation.replaceAll(",", "");
		mathoperation = mathoperation.replaceAll("plus", "+");
		mathoperation = mathoperation.replaceAll("minus", "-");
		mathoperation = mathoperation.replaceAll("times", "*");
		mathoperation = mathoperation.replaceAll("divided by", "/");
		mathoperation = mathoperation.replaceAll("percent of", "percentof");
		mathoperation = mathoperation.replaceAll("to power of", "^");
		mathoperation = mathoperation.replaceAll("sin of", "sin");
		mathoperation = mathoperation.replaceAll("cos of", "cos");
		mathoperation = mathoperation.replaceAll("tan of", "tan");
		mathoperation = mathoperation.replaceAll("inverse sin of", "sin⁻¹");
		mathoperation = mathoperation.replaceAll("inverse cos of", "cos⁻¹");
		mathoperation = mathoperation.replaceAll("inverse tan of", "tan⁻¹");
		String[] splitstr = mathoperation.split(" ");

		while(splitstr.length>1){
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("percentof") >= 0) {
					String buildit = splitstr[i-1] + " percent of " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i-1] = "";
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("^") >= 0) {
					String buildit = splitstr[i-1] + " ^ " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i-1] = "";
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("/") >= 0) {
					String buildit = splitstr[i-1] + " divided by " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i-1] = "";
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("sin⁻¹") >= 0) {
					String buildit = "sin⁻¹ " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("cos⁻¹") >= 0) {
					String buildit = "cos⁻¹ " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("tan⁻¹") >= 0) {
					String buildit = "tan⁻¹ " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("sin") >= 0) {
					String buildit = "sin " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("cos") >= 0) {
					String buildit = "cos " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("tan") >= 0) {
					String buildit = "tan " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("*") >= 0) {
					String buildit = splitstr[i-1] + " * " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i-1] = "";
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf("+") >= 0) {
					String buildit = splitstr[i-1] + " + " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i-1] = "";
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
			for(int i=0; i<splitstr.length; i++) {
				if(splitstr[i].indexOf(" - ") >= 0) {
					String buildit = splitstr[i-1] + " - " + splitstr[i+1];
					String done = math(buildit);
					splitstr[i] = done;
					splitstr[i-1] = "";
					splitstr[i+1] = "";
					ArrayList<String> list = new ArrayList<String>();
					for(String s : splitstr){
						if(!s.equals("")){
							list.add(s);
						}
					}
					splitstr = list.toArray(new String[list.size()]);
				}
			}
		}
		return total;

	}

	public String getOriginalFormula(){
		return originalFormula;
	}
	public String getModifiedFormula(){
		return modifiedFormula;
	}
	private String math(String mathoperation) {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#.#######");
		System.out.println("Math : " + mathoperation);
		if(mathoperation.contains("percent of")){
			mathoperation = mathoperation.replaceAll("percent of", "%");
			int str = mathoperation.indexOf("%");
			fnum = mathoperation.substring(0, str-1);
			fnum = fnum.replaceAll(" ", "");
			fnum = "." + fnum;
			double intfnum = Double.parseDouble(fnum);
			int lastind = mathoperation.length();
			lnum = mathoperation.substring(str+1, lastind);
			lnum = lnum.replaceAll(" ", "");
			double intlnum = Double.parseDouble(lnum);
			double tot = intlnum * intfnum;
			total = df.format(tot);
			if(total.length() == 3){
				total = total + "0";
			}
			if(total.length() > 5){
				total = total.substring(0, 4);
			}
			total = total.replace("0.", "");
		} else 
			if(mathoperation.contains("sin⁻¹")){
				int str = mathoperation.indexOf("sin⁻¹");
				int lastind = mathoperation.length();
				lnum = mathoperation.substring(str + 5, lastind);
				lnum = lnum.replaceAll(" ", "");
				double intlnum = Double.parseDouble(lnum);
				double tot = Math.toDegrees(Math.asin(intlnum)); 
				total = df.format(tot);
			} else 
				if(mathoperation.contains("cos⁻¹")){
					int str = mathoperation.indexOf("cos⁻¹");
					int lastind = mathoperation.length();
					lnum = mathoperation.substring(str + 5, lastind);
					lnum = lnum.replaceAll(" ", "");
					double intlnum = Double.parseDouble(lnum);
					double tot = Math.toDegrees(Math.acos(intlnum)); 
					total = df.format(tot);
				} else 
					if(mathoperation.contains("tan⁻¹")){
						int str = mathoperation.indexOf("tan⁻¹");
						int lastind = mathoperation.length();
						lnum = mathoperation.substring(str + 5, lastind);
						lnum = lnum.replaceAll(" ", "");
						double intlnum = Double.parseDouble(lnum);
						double tot = Math.toDegrees(Math.atan(intlnum)); 
						total = df.format(tot);
					} else 
						if(mathoperation.contains("sin")){
							int str = mathoperation.indexOf("sin");
							int lastind = mathoperation.length();
							lnum = mathoperation.substring(str + 3, lastind);
							lnum = lnum.replaceAll(" ", "");
							double intlnum = Double.parseDouble(lnum);
							double tot = Math.sin(Math.toRadians(intlnum)); 
							total = df.format(tot);
						} else 
							if(mathoperation.contains("cos")){
								int str = mathoperation.indexOf("cos");
								int lastind = mathoperation.length();
								lnum = mathoperation.substring(str + 3, lastind);
								lnum = lnum.replaceAll(" ", "");
								double intlnum = Double.parseDouble(lnum);
								double tot = Math.cos(Math.toRadians(intlnum)); 
								total = df.format(tot);
							} else 
								if(mathoperation.contains("tan")){
									int str = mathoperation.indexOf("tan");
									int lastind = mathoperation.length();
									lnum = mathoperation.substring(str + 3, lastind);
									lnum = lnum.replaceAll(" ", "");
									double intlnum = Double.parseDouble(lnum);
									double tot = Math.tan(Math.toRadians(intlnum)); 
									total = df.format(tot);
								} else 
									if(mathoperation.contains("^")){
										int str = mathoperation.indexOf("^");
										fnum = mathoperation.substring(0, str-1);
										fnum = fnum.replaceAll(" ", "");
										double intfnum = Double.parseDouble(fnum);
										int lastind = mathoperation.length();
										lnum = mathoperation.substring(str+1, lastind);
										lnum = lnum.replaceAll(" ", "");
										double intlnum = Double.parseDouble(lnum);
										double tot = Math.pow(intfnum, intlnum); 
										total = df.format(tot);
									} else 
										if(mathoperation.contains("-")){
											int str = mathoperation.indexOf("-");
											fnum = mathoperation.substring(0, str-1);
											fnum = fnum.replaceAll(" ", "");
											double intfnum = Double.parseDouble(fnum);
											int lastind = mathoperation.length();
											lnum = mathoperation.substring(str+1, lastind);
											lnum = lnum.replaceAll(" ", "");
											double intlnum = Double.parseDouble(lnum);
											double tot = intfnum - intlnum; 
											total = df.format(tot);
										} else 
											if(mathoperation.contains("+")){
												int str = mathoperation.indexOf("+");
												fnum = mathoperation.substring(0, str-1);
												fnum = fnum.replaceAll(" ", "");
												double intfnum = Double.parseDouble(fnum);
												int lastind = mathoperation.length();
												lnum = mathoperation.substring(str+1, lastind);
												lnum = lnum.replaceAll(" ", "");
												double intlnum = Double.parseDouble(lnum);
												double tot = intfnum + intlnum; 
												total = df.format(tot);
											} else 
												if(mathoperation.contains("*")){
													int str = mathoperation.indexOf("*");
													fnum = mathoperation.substring(0, str-1);
													fnum = fnum.replaceAll(" ", "");
													double intfnum = Double.parseDouble(fnum);
													int lastind = mathoperation.length();
													lnum = mathoperation.substring(str+1, lastind);
													lnum = lnum.replaceAll(" ", "");
													double intlnum = Double.parseDouble(lnum);
													double tot = intfnum * intlnum; 
													total = df.format(tot);
												} else
													if(mathoperation.contains("divided by")){
														mathoperation = mathoperation.replaceAll("divided by", "/");
														int str = mathoperation.indexOf("/");
														fnum = mathoperation.substring(0, str-1);
														fnum = fnum.replaceAll(" ", "");
														double intfnum = Double.parseDouble(fnum);
														int lastind = mathoperation.length();
														lnum = mathoperation.substring(str+1, lastind);
														lnum = lnum.replaceAll(" ", "");
														double intlnum = Double.parseDouble(lnum);
														double tot = intfnum / intlnum; 
														total = df.format(tot);
													} else {
														total = null;
													}

		return (total.substring(total.indexOf(".") + 1).length() == 0 ? total.replace(".0", "") : total);
	}

	public String[] getModifiedFormulas() {
		return modifiedFormulas;
	}
}
