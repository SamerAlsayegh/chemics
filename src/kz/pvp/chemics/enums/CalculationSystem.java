package kz.pvp.chemics.enums;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;


public class CalculationSystem {
	HashMap<String, String> formulaInfo = new HashMap<String, String>();
	String[] fieldsOriginal = new String[0];
	String[] formulasOriginal = new String[0];

	String howTo = "";
	String desc = "";
	public CalculationSystem(String Desc, String HowTo, String[] fields, String[] formulasFields) {
		desc = Desc;
		howTo = HowTo;
		fieldsOriginal = fields;
		formulasOriginal = formulasFields;

		if (fields.length == formulasFields.length){
			for (int i = 0; i < formulasFields.length; i++){
				formulaInfo.put(fields[i], formulasFields[i]);
			}
		}
	}
	
	
	public String tryGetFormula(String valueToLookFor){
		for (Entry<String, String> formulaFind : formulaInfo.entrySet()){
			if (formulaFind.getKey().split("-")[0].equalsIgnoreCase(valueToLookFor)){
				return formulaFind.getValue();
			}
		}
		return null;
	}
	
	
	
	public String[] getFields(){
		return fieldsOriginal;
	}
	public String[] getFormulas(){
		return formulasOriginal;
	}
	public String getOriginalFormula(){
		return formulaInfo.get(formulaInfo.keySet().toArray()[0]);
	}
	public String getHowTo(){
		return howTo;
	}
	public String getDesc(){
		return desc;
	}

}
