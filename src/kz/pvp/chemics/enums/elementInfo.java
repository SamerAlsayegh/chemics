package kz.pvp.chemics.enums;



	public class elementInfo{
		private String elementFullName = "";
		private String elementSymbol = "";
		private Integer elementMolarNumber = 0;
		private Double elementMolarMass = 0.0;
		private Integer elementGroup = 0;
		private String elementMP = "";
		private String elementBP = "";
		private String elementDensity = "";
		private String elementEarthCrust = "";
		private String elementDiscovery = "";
		private String elementElectronConfig = "";
		private String elementIonizationEnergy = "";

		
		public elementInfo(String name, String symbol, Integer atomicNumber, Double molarMass, Integer groupNumber, String MP, String BP, String Density, String EarthCrust, String Discovery, String ElectronConfig, String IonizationEnergy){
			elementFullName = name;
			elementSymbol = symbol;
			elementMolarNumber = atomicNumber;
			elementMolarMass = molarMass;
			elementGroup = groupNumber;
			elementMP = MP;
			elementBP = BP;
			elementDensity = Density;
			elementEarthCrust = EarthCrust;
			elementDiscovery = Discovery;
			elementElectronConfig = ElectronConfig;
			elementIonizationEnergy = IonizationEnergy;
		}
		
		public String getFullName(){return elementFullName;}
		public String getMP(){return elementMP;}
		public String getBP(){return elementBP;}
		public String getDensity(){return elementDensity;}
		public String getEarthCrust(){return elementEarthCrust;}
		public String getDiscovery(){return elementDiscovery;}
		public String getElectronConfig(){return elementElectronConfig;}
		public String getSymbol(){return elementSymbol;}
		public String getIonizationEnergy(){return elementIonizationEnergy;}
		public Integer getNumber(){return elementMolarNumber;}
		public Double getMass(){return elementMolarMass;}
		public Integer getGroup(){return elementGroup;}
	}
	