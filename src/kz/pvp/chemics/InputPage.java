package kz.pvp.chemics;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import kz.pvp.chemics.MainMenu.TrackerName;
import kz.pvp.chemics.enums.CalculationSystem;
import kz.pvp.chemics.enums.Formula;
import kz.pvp.chemics.enums.inputType;
import kz.pvp.chemics.enums.pageType;
import kz.pvp.chemics.physics.Calculations;
import kz.pvp.chemics.utilities.Messages;
import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Sensor;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class InputPage extends Fragment{
	private static ListView myList;
	private static MyAdapter myAdapter;
	public static LinkedHashMap<String, inputType> inputs = new LinkedHashMap<String, inputType>();
	public static LinkedHashMap<Integer, String> presetInfo = new LinkedHashMap<Integer, String>();
	public LinkedHashMap<String, inputType>  customFields = new LinkedHashMap<String, inputType> ();
	public static String presetNameField = "Field";
	public static ArrayList<String> arrayOptions = new ArrayList<String>();
	public static HashSet<Integer> inputIds = new HashSet<Integer>();
	public static HashMap<String, String> formulas = new HashMap<String, String>();
	//public static LinkedHashMap<String, CalculationSystem>  formulaInfo = new LinkedHashMap<String, CalculationSystem> ();
	public static LinkedHashMap<String, CalculationSystem>  formulaInfo = new LinkedHashMap<String, CalculationSystem> ();
	public static HashMap<Integer, View> views = new HashMap<Integer, View>();

	public static InputPage instance = null;
	private static String title;// The title of the page
	private static String moreInfo;// More info about the calculation
	private static String howTo;// How to solve the question.
	Sensor mSensor;
	/** Called when the activity is first created. */
	public static final ArrayList<ListItem> myItems = new ArrayList<ListItem>();


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_inputs_page, container, false);

		// Start loading the ad in the background.
		instance = this;
		return view;
	}

	public static void showPopup() {

		View popupView = instance.getActivity().getLayoutInflater().inflate(R.layout.popout_howto, null);

		PopupWindow popupWindow = new PopupWindow(popupView, 
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		// Example: If you have a TextView inside `popup_layout.xml`    
		TextView tv = (TextView) popupView.findViewById(R.id.howto);

		tv.setText(howTo);

		// If the PopupWindow should be focusable
		popupWindow.setFocusable(true);

		// If you need the PopupWindow to dismiss when when touched outside 
		popupWindow.setBackgroundDrawable(new ColorDrawable());

		int location[] = new int[2];

		// Get the View's(the one that was clicked in the Fragment) location
		View mainView = null;
		mainView = MainMenu.instance.findViewById(R.id.inputPage);
		System.out.println("Ok found: " + mainView + " and " + location);

		mainView.getLocationOnScreen(location);

		// Using location, the PopupWindow will be displayed right under anchorView
		popupWindow.showAtLocation(mainView, Gravity.CENTER, 
				location[0], location[1]);

	}

	public static void setupFragment(String val){
		formatPage(val);
		System.out.println("ddd");
		myList = (ListView) instance.getView().findViewById(R.id.inputPage);
		TextView title = (TextView) instance.getView().findViewById(R.id.titleText);
		title.setText(InputPage.title);
		TextView desc = (TextView) instance.getView().findViewById(R.id.infoText);
		desc.setText(InputPage.moreInfo);

		myList.setItemsCanFocus(true);
		myAdapter = new MyAdapter();
		myList.setAdapter(myAdapter);
		AdRequest adRequest = new AdRequest.Builder()
		.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
		.build();
		MainMenu.AdFragment.mAdView.loadAd(adRequest);
	}

	public static void getAnswer(){
		String answer = null;
		boolean alreadyRun = false;
		Integer lastStep = 0;
		String error = "";
		ArrayList<String> answers = new ArrayList<String>();
		System.out.println("Debug 1");
		if (formulaInfo.containsKey(SubDetails.pT) && formulaInfo.containsKey(title)){
			System.out.println("Debug 2");
			CalculationSystem cal = formulaInfo.get(title);
			String formula = null;
			boolean pass = true;
			String missingVar = "";
			for (Integer id : inputIds){
				EditText text = (EditText)instance.getView().findViewById(id);
				if (text != null){
					if (text.getText().toString().isEmpty()){// We found an empty field, so the person is looking for something.
						if (formula != null)
							pass = false;
						formula = cal.tryGetFormula(text.getHint().toString());// We get formula looking for the value that is missing.
						missingVar = text.getHint().toString();
					}
				}
			}
			if (pass == true && formula != null){
				ArrayList<String> vals = new ArrayList<String>();
				int sigDigits = 100;
				for (Integer id : inputIds){
					EditText text = (EditText)instance.getView().findViewById(id);
					if (text != null){
						if (text.getHint().toString() != null){
							for (String field : cal.getFields()){
								if (!text.getText().toString().isEmpty()){
									if (field.contains(text.getHint().toString())){
										vals.add(field.split("-")[1] + ":" + text.getText().toString());
										int sig = significantDigits(new BigDecimal(Double.parseDouble(text.getText().toString()), MathContext.DECIMAL64));
										System.out.println("Sig for " + text.getHint().toString() + " is  " + sig);
										if (sigDigits >= sig)
											sigDigits = sig;
										break;
									}
								}
							}
						}
					}
				}
				Formula f = new Formula(formula, (String[]) vals.toArray(new String[vals.size()]));

				String val = "";
				Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(missingVar);
				if (m.find())
					val = " "+ m.group(1);
				String ans = f.Math(null);
				String sigDigit = getSigNum(ans, sigDigits);
				if (sigDigit != null){
					answers.add("How To Answer");
					int number = 0;
					for (int x = 0; x < cal.getFields().length; x++){
						if (cal.getFields()[x].split("-")[0].equalsIgnoreCase(missingVar.split("-")[0]))
							number = x;
					}

					answers.add("1|    " + cal.getFields()[0].toString().split("-")[1] + " = " + cal.getFormulas()[0]);
					answers.add("2|    " + cal.getFields()[number].toString().split("-")[1] + " = " + cal.getFormulas()[number]);
					for (int x = 0; x < f.getModifiedFormulas().length; x++)
						answers.add((x+3) +"|    " + cal.getFields()[number].toString().split("-")[1] + " = " + f.getModifiedFormulas()[x]);
					answers.add((f.getModifiedFormulas().length +3) + "|    " + cal.getFields()[number].toString().split("-")[1] + " = " + ans);
					lastStep = f.getModifiedFormulas().length + 6;
					
					answers.add("The " + missingVar.replace("(" + val + ")", "") + " is");
					answer = ans + val + "\n" + sigDigit + val;
				}
				else{
					answers.add("Calculation Error...");
					answers.add("Please use other numbers...");
					answer = "N/A";
				}

			}
			else if (missingVar.isEmpty())
				error = "No target value selected.";
		}
		else {
			if (title.equalsIgnoreCase(Messages.MotionEquationTitle)){
				String Vi = "";
				String Vf = "";
				String T = "";
				String A = "";
				String D = "";
				for (Integer id : inputIds){
					EditText text = (EditText)instance.getView().findViewById(id);
					if (text != null){
						if (text.getHint().toString().equalsIgnoreCase("Initial Velocity (m/s)"))
							Vi = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Final Velocity (m/s)"))
							Vf = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Acceleration (m/s²)"))
							A = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Time (s)"))
							T = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Distance (m)"))
							D = text.getText().toString();
					}
				}
				answer = Calculations.getMotionEq1(Vi, Vf, A, T);
				if (answer == null)
					answer = Calculations.getMotionEq2(Vi, Vf, D, T);
				else if (answer == null)
					answer = Calculations.getMotionEq3(Vi, A, D, T);
				else if (answer == null)
					answer = Calculations.getMotionEq4(Vf, A, D, T);
				else if (answer == null)
					answer = Calculations.getMotionEq5(Vf, A, D, Vi);
			}
			else if (title.equalsIgnoreCase(Messages.NumberAtomsTitle)){
				String N = "";
				String moles = "";
				for (Integer id : inputIds){
					EditText text = (EditText)instance.getView().findViewById(id);
					if (text.getHint().toString().equalsIgnoreCase("Moles"))
						moles = text.getText().toString();
					else if (text.getHint().toString().equalsIgnoreCase("Atoms/Molecules"))
						N = text.getText().toString();
				}
				answer = Calculations.numberOfAtoms(moles, N);
			}
			else if (title.equalsIgnoreCase(Messages.DilutionTitle)){
				String C1 = "";
				String C2 = "";
				String V1 = "";
				String V2 = "";
				for (Integer id : inputIds){
					EditText text = (EditText)instance.getView().findViewById(id);
					if (text.getHint().toString().equalsIgnoreCase("Concentration ₁(mol/L)"))
						C1 = text.getText().toString();
					else if (text.getHint().toString().equalsIgnoreCase("Volume ₁(L)"))
						V1 = text.getText().toString();
					else if (text.getHint().toString().equalsIgnoreCase("Concentration ₂(mol/L)"))
						C2= text.getText().toString();
					else if (text.getHint().toString().equalsIgnoreCase("Volume ₂(L)"))
						V2 = text.getText().toString();
				}
				answer = Calculations.Dilution(C1, C2, V1, V2);
			}
			else if (title.equalsIgnoreCase(Messages.DopplerEffectTitle)){
				String Fd = "";
				String Fs = "";
				String Vw = "";
				String Vs = "";
				boolean approach = false;
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("Doppler Frequency (hertz)"))
							Fd = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Source Frequency (hertz)"))
							Fs = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Velocity of Sound in Air (m/s)"))
							Vw= text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Velocity of Source (m/s)"))
							Vs = text.getText().toString();
					}
					else if (instance.getView().findViewById(id) instanceof Spinner){
						Spinner dropdown = (Spinner)instance.getView().findViewById(id);
						String value = dropdown.getSelectedItem().toString();

						if (value.equalsIgnoreCase("Approaching"))
							approach = true;
					}
				}
				answer = Calculations.doppler(Fd, Fs, Vw, Vs, approach);
			}
			else if (title.equalsIgnoreCase(Messages.ConcentrationTitle)){
				String c = "";
				String n = "";
				String V = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("Concentration (mol/L)"))
							c = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Amount (mol)"))
							n = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Volume (L)"))
							V = text.getText().toString();
					}
				}
				answer = Calculations.Concentration(c,n,V);
			}
			else if (title.equalsIgnoreCase(Messages.ProjectileTitle)){
				String Theta = "";
				String V = "";
				String Vz = "";
				boolean vertical = false;
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("Angle(°)"))
							Theta = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Velocity (m/s)"))
							V = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Velocity of Component (m/s)"))
							Vz = text.getText().toString();
					}
					else if (instance.getView().findViewById(id) instanceof Spinner){
						Spinner dropdown = (Spinner)instance.getView().findViewById(id);
						String value = dropdown.getSelectedItem().toString();

						if (value.equalsIgnoreCase("Vertical Component"))
							vertical = true;
					}
				}
				answer = Calculations.Projectile(Theta, V, Vz, vertical);
			}
			else if (title.equalsIgnoreCase(Messages.ThermalEnergyTitle)){
				String Q = "";
				String m = "";
				String c = "";
				String tD = "";

				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("Thermal Energy(jules)"))
							Q = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Mass(kg)"))
							m = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Thermal Capacity(kJ/(kg*K)"))
							c = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Temperature Difference(°C)"))
							tD = text.getText().toString();
					}
				}
				answer = Calculations.ThermalEnergy(Q, m, c, tD);
			}
			else if (title.equalsIgnoreCase(Messages.LengthBetweenPointsTitle)){
				String x1 = "";
				String y1 = "";
				String x2 = "";
				String y2 = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("X₁ Co-ord"))
							x1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("X₂ Co-ord"))
							x2 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Y₁ Co-ord"))
							y1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Y₂ Co-ord"))
							y2 = text.getText().toString();
					}
				}
				answer = Calculations.getLength(x1, x2, y1, y2);
			}
			else if (title.equalsIgnoreCase(Messages.MidpointTitle)){
				String x1 = "";
				String y1 = "";
				String x2 = "";
				String y2 = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("X₁ Co-ord"))
							x1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("X₂ Co-ord"))
							x2 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Y₁ Co-ord"))
							y1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Y₂ Co-ord"))
							y2 = text.getText().toString();
					}
				}
				answer = Calculations.getMidpoint(x1, x2, y1, y2);
			}
			else if (title.equalsIgnoreCase(Messages.CosineLawTitle)){
				String sideA = "";
				String sideB = "";
				String sideC = "";
				String Angle = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("Side X"))
							sideA = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Side Y"))
							sideB = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Side Z"))
							sideC = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Angle(°)"))
							Angle = text.getText().toString();
					}
				}
				answer = Calculations.cosLaw(sideA, sideB, sideC, Angle);
			}
			else if (title.equalsIgnoreCase(Messages.CombinedGasLawTitle)){
				String t1 = "";
				String p1 = "";
				String v1 = "";
				String t2 = "";
				String p2 = "";
				String v2 = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("Temperature ₁(°K)"))
							t1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Temperature ₂(°K)"))
							t2 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Volume ₁"))
							v1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Volume ₂"))
							v2 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Pressure ₁"))
							p1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Pressure ₂"))
							p2 = text.getText().toString();
					}
				}
				answer = Calculations.combLaw(p1, v1, t1, p2, v2, t2);
			}
			else if (title.equalsIgnoreCase(Messages.MolarMassCombinedTitle)){
				String equation = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("Elements Equation"))
							equation = text.getText().toString();
					}
				}
				answer = Calculations.molarMass(equation);
				if (answer == null){
					alreadyRun = true;
					Toast.makeText(instance.getActivity(), "The Equation/Elements are wrong.", Toast.LENGTH_LONG).show();
				}
			}
			else if (title.equalsIgnoreCase(Messages.SnellsLawTitle)){
				String n1 = "";
				String sin1 = "";
				String n2 = "";
				String sin2 = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("n₁"))
							n1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Sinθ₁"))
							sin1 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("n₂"))
							n2 = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("Sinθ₂"))
							sin2 = text.getText().toString();
					}
				}
				answer = Calculations.snellsLaw(n1,sin1,n2,sin2);

			}
			else if (title.equalsIgnoreCase(Messages.ResistanceParallelTitle)){
				ArrayList<String> entries = new ArrayList<String>();
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText) instance.getView().findViewById(id);
						if (!text.getText().toString().isEmpty())
							entries.add(text.getText().toString());
					}
				}
				answer = Calculations.resistanceParallel(entries);

			}
			else if (title.equalsIgnoreCase(Messages.ResistanceSeriesTitle)){
				ArrayList<String> entries = new ArrayList<String>();
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText) instance.getView().findViewById(id);
						if (!text.getText().toString().isEmpty())
							entries.add(text.getText().toString());
					}
				}
				answer = Calculations.resistanceSeries(entries);
			}
			else if (title.equalsIgnoreCase(Messages.OhmsLawTitle)){
				String v = null, i = null,r = null;
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText) instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("V (Voltage)"))
							v = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("I (Amperes)"))
							i = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("R (Ohms)"))
							r = text.getText().toString();
					}
				}
				answer = Calculations.ohmsLaw(v, i, r);
			}
			else if (title.equalsIgnoreCase(Messages.QuadraticTitle)){
				String A = "";
				String B = "";
				String C = "";
				for (Integer id : inputIds){
					if (instance.getView().findViewById(id) instanceof EditText){
						EditText text = (EditText)instance.getView().findViewById(id);
						if (text.getHint().toString().equalsIgnoreCase("a"))
							A = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("b"))
							B = text.getText().toString();
						else if (text.getHint().toString().equalsIgnoreCase("c"))
							C = text.getText().toString();
					}
				}
				answer = Calculations.quadFormula(A,B,C);
				if (answer != null){
					alreadyRun = true;
					String ans = "";
					if (answer.contains(":")){
						String[] answerSplit = answer.split(":");
						if (answerSplit.length > 2)
							ans = "Zero 1: " + answerSplit[1] + " | Zero 2: " + answerSplit[2];
						else
							ans = "Zero 1: " + answerSplit[1];
					}
					else
						ans = "No zeros";
					answers.add("The zero result is ");
					answers.add(ans);
					//Toast.makeText(instance, "The " + var + " is " + number, Toast.LENGTH_LONG).show();			
					showPopup(answers, lastStep);		
				}
			}
		}
		if (error.isEmpty())
			error = "Values are incorrectly filled";
		if (alreadyRun == false){
			if (answer != null){
				String[] answerSplitString = answer.split("/n");
				for (String answerLine : answerSplitString)
					answers.add(answerLine);
				showPopup(answers, lastStep);
			}
			else
				Toast.makeText(instance.getActivity(), error, Toast.LENGTH_SHORT).show();
		}
	}

	public static BigDecimal round(BigDecimal value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = value;
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd;
	}
	public static BigDecimal roundFull(BigDecimal ans, int places){
		places++;
		String answer = ans.toString()+"";
		BigDecimal value = new BigDecimal("0.0");
		if (answer.contains(".")){
			Integer indexDot = answer.indexOf(".");
			String val = answer.replace(".", "");
			value = new BigDecimal("0." + val);
			ans = round(value, places);
			answer = (""+ans).replace(".", "");
			if (answer.length() > indexDot)
				answer = answer.substring(0, indexDot+1) + "." + answer.substring(indexDot+1);
			ans = new BigDecimal(answer);
		}
		else
			ans = round(value, places);

		return round(ans, places - 1);
	}


	private static String getSigNum(String answer, int sigDigits) {
		if (!answer.equalsIgnoreCase("NaN") || sigDigits == 0){
			System.out.println("And the winner is: " + answer);
			BigDecimal ans = new BigDecimal(answer);
			ans = roundFull(ans, sigDigits - 1);
			answer = ans+"";
			System.out.println("Out: " + answer);
			if (ans.doubleValue() < 1 && ans.doubleValue() > -1){
				answer = answer.replace(".", "");
				String numbs = getFirstNumbs(answer);
				String val = numbs.substring(0, 1) + "." + numbs.substring(1) + " x10 ^ -" + (sigDigits);
				return val;
			}
			else {

				int totalChars = 0;
				if (answer.contains("."))
					totalChars = answer.split("\\.")[0].length() - 1;
				else
					totalChars = answer.length() - 1;
				String answerStrip = answer.replace(".", "");
				String val = (answerStrip.length() == sigDigits ? answer : (answerStrip.substring(0, 1) + "." + answerStrip.substring(1) + " x10 ^ " + totalChars));
				return val;
			}
		}
		else
			return null;
	}

	private static String getFirstNumbs(String answer) {
		answer = answer.replace(".", "");
		int loc = 0;
		for (int x = 0; x < answer.length(); x++){
			if (answer.charAt(x) != '0'){
				loc = x;
				break;
			}
			System.out.println("Ok " + loc);
		}
		System.out.println("Ok " + answer);
		return answer.substring(loc);
	}

	private static String getZero(String ans) {
		return new String(new char[ans.length()]).replace("\0", "0");
	}

	public static void showPopup(final ArrayList<String> answers, final Integer lastStep) {
		// our adapter instance
		
		// our adapter instance
		ArrayAdapter adapter = new ArrayAdapter(instance.getActivity(), R.layout.text_item, answers);

		// create a new ListView, set the adapter and item click listener
		ListView listViewItems = new ListView(instance.getActivity());
		listViewItems.setDividerHeight(0);
		listViewItems.setAdapter(adapter);
		listViewItems.setBackgroundResource(R.drawable.button_round_border_white_light);
		// put the ListView in the pop up
		listViewItems.setPadding(20, 20, 20, 20);
		Dialog dialog = new Dialog(instance.getActivity(), R.style.NewDialog);
		dialog.setContentView(listViewItems);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
		
		listViewItems.setTextFilterEnabled(true);
		listViewItems.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				ArrayList<String> list = new ArrayList<String>();
				if (position == 1)
					list.add("We re-state the original equation in Physics.");
				else if (position == 2)
					list.add("We re-arrange for the value we are looking for.");
				else if (position == 3)
					list.add("Now, we plug-in the values we know.");
				else if (position >= 4 && position < lastStep-2)
					list.add("Now, each step is just simplifying the equation...");
				else if (position >= lastStep - 2){
					list.add("This is part of the final solution...");
				}
				showPopupDetails(list);
			}
		});
	}
	
	public static void showPopupDetails(final ArrayList<String> answers) {
		// our adapter instance
		ArrayAdapter adapter = new ArrayAdapter(instance.getActivity(), R.layout.text_item_centered, answers);
		
		// create a new ListView, set the adapter and item click listener
		ListView listViewItems = new ListView(instance.getActivity());
		listViewItems.setDividerHeight(0);
		listViewItems.setAdapter(adapter);
		listViewItems.setBackgroundResource(R.drawable.button_round_border_white_light);
		// put the ListView in the pop up
		listViewItems.setPadding(20, 20, 20, 20);
		Dialog dialog = new Dialog(instance.getActivity(), R.style.NewDialog);
		dialog.setContentView(listViewItems);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}
	private static int significantDigits(BigDecimal input) {
		return input.scale() <= 0
				? input.precision() + input.stripTrailingZeros().scale()
						: input.precision();
	}

	public static class MyAdapter extends BaseAdapter {
		private LayoutInflater mInflater;
		public static HashMap<Integer, String> posValues = new HashMap<Integer, String>();
		public MyAdapter() {
			mInflater = (LayoutInflater) instance.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			myItems.clear();
			posValues.clear();
			for (Entry<String, inputType> entry  : InputPage.inputs.entrySet()) {
				ListItem listItem = instance.new ListItem();
				listItem.caption = entry.getKey();
				myItems.add(listItem);
			}
			notifyDataSetChanged();
		}

		public int getCount() {
			return myItems.size();
		}

		public Object getItem(int position) {
			return position;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView, ViewGroup parent) {
			final ViewHolder holder;
			holder = instance.new ViewHolder();
			if (myItems.get(position).caption.equalsIgnoreCase("Spinner")){
				convertView = mInflater.inflate(R.layout.dropdownitem, null);
				holder.dropdown = (Spinner) convertView.findViewById(R.id.ItemDropdown);
			}
			else if (myItems.get(position).caption.equalsIgnoreCase("FieldAdder")){
				convertView = mInflater.inflate(R.layout.add_fielditem, null);
				holder.add = (Button) convertView.findViewById(R.id.addField);
				holder.remove = (Button) convertView.findViewById(R.id.removeField);
			}
			else{
				convertView = mInflater.inflate(R.layout.item, null);
				holder.caption = (EditText) convertView.findViewById(R.id.ItemCaption);
				if (posValues.containsKey(position))
					holder.caption.setText(posValues.get(position));
				System.out.println("Init "  + position);
			}
			convertView.setTag(holder);
			//Fill EditText with the value you have in data source
			if (myItems.get(position).caption.equalsIgnoreCase("Spinner")){
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(instance.getActivity(),
						android.R.layout.simple_spinner_dropdown_item, arrayOptions);
				// Specify the layout to use when the list of choices appears
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				holder.dropdown.setAdapter(adapter);
				holder.dropdown.setId(position);
			}
			else if (!myItems.get(position).caption.equalsIgnoreCase("FieldAdder")){
				if (holder.caption != null){
					holder.caption.setHint(myItems.get(position).caption);
					holder.caption.setId(position);
					if (inputs.containsKey(myItems.get(position).caption)){
						if (inputs.get(myItems.get(position).caption).equals(inputType.String) && holder.caption.getInputType() != InputType.TYPE_CLASS_TEXT){
							holder.caption.setInputType(InputType.TYPE_CLASS_TEXT);
						}
					}


					holder.caption.setOnFocusChangeListener(new OnFocusChangeListener() {
						public void onFocusChange(View v, boolean hasFocus) {
							if (!myItems.get(position).caption.equalsIgnoreCase("Spinner") && !myItems.get(position).caption.equalsIgnoreCase("FieldAdder")){
								final int position = v.getId();
								if (!hasFocus){
									holder.caption.setHint(myItems.get(position).caption);
									if (!holder.caption.getText().toString().isEmpty()){
										posValues.put(position, holder.caption.getText().toString());
									}
								}
							}
						}
					});

					holder.caption.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							ListView listView = myList;
							if (position == 1)
							{
								listView.setItemsCanFocus(true);

								// Use afterDescendants, because I don't want the ListView to steal focus
								listView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
								if (getItem(position) instanceof EditText){
									EditText text = (EditText) getItem(position);
									text.requestFocus();
								}
							}
							else
							{
								if (!listView.isFocused())
								{
									// listView.setItemsCanFocus(false);

									// Use beforeDescendants so that the EditText doesn't re-take focus
									listView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
									listView.requestFocus();
								}
							}
						}
					});
					System.out.println("Pos "  + position);
				}
			}
			if (!inputIds.contains(position))
				inputIds.add(position);
			//we need to update adapter once we finish with editing
			return convertView;
		}

		public void onNothingSelected(AdapterView<?> listView)
		{
			// This happens when you start scrolling, so we need to prevent it from staying
			// in the afterDescendants mode if the EditText was focused 
			listView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
		}
	}

	class ViewHolder {
		EditText caption;
		Spinner dropdown;
		Button add;
		Button remove;
	}

	class ListItem {
		String caption;
	}

	public void addField(View view){
		if (customFields.size() < 20){
			inputs.put(presetNameField + (customFields.size() + 1), inputType.Number);
			customFields.put(presetNameField + (customFields.size() + 1), inputType.Number);
			myAdapter = new MyAdapter();
			myList.setAdapter(myAdapter);
			//Intent intent = new Intent(instance, InputPage.class);
			//instance.startActivity(intent);
		}
	}


	public void removeField(View view){
		if (customFields.size() >= 1){
			inputs.remove(presetNameField + (customFields.size()));
			customFields.remove(presetNameField + (customFields.size()));
			System.out.println("Size = " + customFields.size());
			myAdapter = new MyAdapter();
			myList.setAdapter(myAdapter);

			//Intent intent = new Intent(instance, InputPage.class);

			//instance.startActivity(intent);
		}
	}
	public static void formatPage(String val) {
		InputPage.title = val;
		InputPage.inputs.clear();
		String desc = "Input the values you have.";
		String howto = "Unknown";

		Tracker t = MainMenu.getTracker(
				TrackerName.APP_TRACKER);
		t.setScreenName(SubDetails.pT.toString() + " - " + val);
		t.send(new HitBuilders.ScreenViewBuilder().build());

		if (val.equalsIgnoreCase(Messages.MotionEquationTitle)){
			desc = Messages.MotionEquationDesc;
			howto = Messages.MotionEquationFormula;
			addCalculation(desc, howto, "Initial Velocity (m/s)", "Final Velocity (m/s)", "Acceleration (m/s²)", "Time (s)", "Distance (m)");
		}
		else if (val.equalsIgnoreCase(Messages.DilutionTitle)){
			desc = Messages.DilutionDesc;
			howto = Messages.DilutionFormula;
			addCalculation(desc, howto, "Concentration ₁(mol/L)", "Volume ₁(L)", "Concentration ₂(mol/L)", "Volume ₂(L)");
		}
		else if (val.equalsIgnoreCase(Messages.DopplerEffectTitle)){
			desc = Messages.DopplerEffectDesc;
			howto = Messages.DopplerEffectFormula;
			InputPage.inputs.put("Spinner", inputType.Number);
			addCalculation(desc, howto, "Doppler Frequency (hertz)", "Source Frequency (hertz)", "Velocity of Sound in Air (m/s)", "Velocity of Source (m/s)");
			arrayOptions.add("Receding");
			arrayOptions.add("Approaching");
		}
		else if (val.equalsIgnoreCase(Messages.QuadraticTitle)){
			desc = Messages.QuadraticDesc;
			howto = Messages.QuadraticFormula;
			addCalculation(desc, howto, "a", "b", "c");

		}
		else if (val.equalsIgnoreCase(Messages.ProjectileTitle)){
			desc = Messages.ProjectileDesc;
			howto = Messages.ProjectileFormula;
			InputPage.inputs.put("Spinner", inputType.Number);
			addCalculation(desc, howto, "Angle(°)", "Velocity (m/s)", "Velocity of Component (m/s)");

			arrayOptions.clear();
			arrayOptions.add("Horizontal Component");
			arrayOptions.add("Vertical Component");
		}
		else if (val.equalsIgnoreCase(Messages.ThermalEnergyTitle)){
			desc = Messages.ThermalEnergyDesc;
			howto = Messages.ThermalEnergyFormula;
			addCalculation(desc, howto, "Thermal Energy(Jules)", "Mass(kg)", "Thermal Capacity(kJ/(kg*K)", "Temperature Difference(°C)");
		}
		else if (val.equalsIgnoreCase(Messages.LengthBetweenPointsTitle)){
			desc = Messages.LengthBetweenPointsDesc;
			howto = Messages.LengthBetweenPointsHowto;
			addCalculation(desc, howto, "X₁ Co-ord", "Y₁ Co-ord", "X₂ Co-ord", "Y₂ Co-ord");
		}
		else if (val.equalsIgnoreCase(Messages.MidpointTitle)){
			desc = Messages.MidpointDesc;
			howto = Messages.MidpointFormula;
			addCalculation(desc, howto, "X₁ Co-ord", "Y₁ Co-ord", "X₂ Co-ord", "Y₂ Co-ord");
		}
		else if (val.equalsIgnoreCase(Messages.CosineLawTitle)){
			desc = Messages.CosineLawDesc;
			howto =  Messages.CosineLawFormula;
			addCalculation(desc, howto, "Side X", "Side Y", "Side Z", "Angle(°)");
		}
		else if (val.equalsIgnoreCase(Messages.CombinedGasLawTitle)){
			desc = Messages.CombinedGasLawDesc;
			howto = Messages.CombinedGasLawFormula;
			addCalculation(desc, howto, "Pressure ₁", "Volume ₁", "Temperature ₁(°K)", "Pressure ₂", "Volume ₂", "Temperature ₂(°K)");
		}
		else if (val.equalsIgnoreCase(Messages.MolarMassCombinedTitle)){
			desc = Messages.MolarMassCombinedDesc;
			howto = Messages.MolarMassCombinedFormula;
			addCalculation(desc, howto, "Elements Equation[S]");
		}
		else if (val.equalsIgnoreCase(Messages.SnellsLawTitle)){
			desc = Messages.SnellsLawDesc;
			howto = Messages.SnellsLawFormula;
			addCalculation(desc, howto, "n₁", "Sinθ₁", "n₂", "Sinθ₂");
		}
		else if (val.equalsIgnoreCase(Messages.ResistanceParallelTitle)){
			desc = Messages.ResistanceParallelDesc;
			howto = Messages.ResistanceParallelFormula;
			addCalculation(desc, howto, "FieldAdder");
			presetNameField = "R";
		}
		else if (val.equalsIgnoreCase(Messages.ResistanceSeriesTitle)){
			desc = Messages.ResistanceSeriesDesc;
			howto = Messages.ResistanceSeriesFormula;
			addCalculation(desc, howto, "FieldAdder");
			presetNameField = "R";
		}
		else if (val.equalsIgnoreCase(Messages.OhmsLawTitle)){
			desc = Messages.OhmsLawDesc;
			howto = Messages.OhmsLawFormula;
			addCalculation(desc, howto, "V (Voltage)", "I (Amperes)", "R (Ohms)");
		}
		else if (formulaInfo.containsKey(val))
			addCalculation(formulaInfo.get(val));

		System.out.println("Val "  + val);
		InputPage.title = val;
		InputPage.howTo = howto;
		InputPage.moreInfo = desc;
	}


	@Deprecated
	public static void addCalculation(String desc, String howToFormula, String... Inputs){
		moreInfo = desc;
		howTo = howToFormula;
		for (String entryName : Inputs){
			if (entryName.contains("[S]"))
				inputs.put(entryName.replace("[S]", ""), inputType.String);
			else
				inputs.put(entryName, inputType.Number);
		}
	}


	public static void addCalculation(CalculationSystem cal){
		inputs.clear();
		moreInfo = cal.getDesc();
		howTo = cal.getHowTo();
		for (String field : cal.getFields()){
			if (field.contains("-[S]")){
				field.replace("-[S]", "");
				if (field.contains("-"))
					field = field.split("-")[0];
				inputs.put(field, inputType.String);
			}
			else{
				if (field.contains("-"))
					field = field.split("-")[0];
				inputs.put(field, inputType.Number);
			}
		}
	}
}
