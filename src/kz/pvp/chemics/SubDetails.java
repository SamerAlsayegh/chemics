package kz.pvp.chemics;

import java.util.ArrayList;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import kz.pvp.chemics.MainMenu.TrackerName;
import kz.pvp.chemics.chemistry.periodicTable;
import kz.pvp.chemics.enums.OnSwipeTouchListener;
import kz.pvp.chemics.enums.elementInfo;
import kz.pvp.chemics.enums.pageType;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ListActivity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;


public class SubDetails extends ListFragment {
	static pageType pT = pageType.About_Us;
	public static ArrayList<String> listOptions = new ArrayList<String>();
	public static SubDetails instance = null;


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		instance = this;
	}
	
	public static void setupFragment(){
		instance.setListAdapter(new ArrayAdapter<String>(instance.getActivity(), R.layout.activity_list_page, listOptions));		

		/*
		 * 		if (pT == pageType.Chemistry)
			getWindow().getDecorView().setBackgroundResource(R.drawable.background_chemistry_drawable);
		else if (pT == pageType.Physics)
			getWindow().getDecorView().setBackgroundResource(R.drawable.background_physics_drawable);
		else if (pT == pageType.Math)
			getWindow().getDecorView().setBackgroundResource(R.drawable.background_math_drawable);
		 */


		Tracker t = MainMenu.getTracker(
				TrackerName.APP_TRACKER);
		t.setScreenName(pT.toString() + " - Menu");
		t.send(new HitBuilders.ScreenViewBuilder().build());

		instance.setListAdapter(new ArrayAdapter<String>(instance.getActivity(), R.layout.activity_list_page, listOptions));		

		final ListView listView = instance.getListView();
		listView.setTextFilterEnabled(true);		
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String val = listOptions.get(position);

				if (pT.equals(pageType.Chemistry)){
					if (val.equalsIgnoreCase("Periodic Table")){
						
						listOptions.clear();
						if (listOptions.size() <= 0)
						for (elementInfo elementInfo : periodicTable.elements.values()){
							listOptions.add(elementInfo.getNumber() + " - " + elementInfo.getFullName());
						}
						instance.setListAdapter(new ArrayAdapter<String>(instance.getActivity(), R.layout.activity_list_page, listOptions));
						
						
						final ListView listView = instance.getListView();
						listView.setTextFilterEnabled(true);

						listView.setOnItemClickListener(new OnItemClickListener() {
							public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
								String val = listOptions.get(position);
								if (periodicTable.elements.containsKey(val.split(" - ")[1])){
									elementInfo details = periodicTable.elements.get(val.split(" - ")[1]);
									//showDialog(details);
									showPopup(details);
								}
							}
						});
					}
					else{
						//Intent intent = new Intent(instance, InputPage.class);
						InputPage.inputs.clear();
						InputPage.inputIds.clear();
						InputPage.myItems.clear();
						InputPage.setupFragment(val);
						//startActivity(intent);
						//overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
						MainMenu.toggleLayout(R.id.inputs, MainMenu.openID);

					}
				}
				else if (pT.equals(pageType.Physics) || pT.equals(pageType.Math)){
					//Intent intent = new Intent(instance, InputPage.class);
					InputPage.inputs.clear();
					InputPage.inputIds.clear();
					InputPage.myItems.clear();
					InputPage.setupFragment(val);
					MainMenu.toggleLayout(R.id.inputs, MainMenu.openID);
					//startActivity(intent);
					//overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);


				}
				MainMenu.deepLevel = 2;
			}
		});
	}
	
	public static void showPopup(elementInfo details) {
		InfoPage.setupInfo(details);
		// our adapter instance
		ArrayAdapter adapter = new ArrayAdapter(instance.getActivity(), R.layout.text_item, InfoPage.details);

		// create a new ListView, set the adapter and item click listener
		ListView listViewItems = new ListView(instance.getActivity());
		listViewItems.setDividerHeight(0);
		listViewItems.setAdapter(adapter);
		listViewItems.setBackgroundResource(R.drawable.button_round_border_green_light);
		// put the ListView in the pop up
		listViewItems.setPadding(20, 20, 20, 20);
		Dialog dialog = new Dialog(instance.getActivity(), R.style.NewDialog);
		dialog.setContentView(listViewItems);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}
	

	/*
	 * 	@Override
	public void onBackPressed() {
	    super.onBackPressed();
		this.overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
	}
	 */
}