package kz.pvp.chemics.chemistry;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;

import kz.pvp.chemics.SubDetails;
import kz.pvp.chemics.enums.elementInfo;

public class periodicTable {
	public static LinkedHashMap<String, elementInfo> elements = new LinkedHashMap<String, elementInfo>();







	public static void setUpElements() {
		elements.clear();
		Scanner scanner = new Scanner(periodicTable.class.getResourceAsStream("elements.data"));
		String elementLine = "";
		if (scanner != null){
			System.out.println("STARTED: Elements setup");
			while (scanner.hasNext()){
				elementLine = scanner.nextLine();
				String[] elementInfoSplit = elementLine.split(":");
				Integer elementNumber= Integer.parseInt(elementInfoSplit[0]);
				Double elementMass = Double.parseDouble(elementInfoSplit[1]);
				String elementName = elementInfoSplit[2];
				System.out.println("Setting up elements" + elementName);
				String elementSymbol = elementInfoSplit[3];
				String meltingPoint = elementInfoSplit[4];
				String boilingPoint = elementInfoSplit[5];
				String density = elementInfoSplit[6];
				String earthCrustPercent = elementInfoSplit[7];
				String discoveryYear = elementInfoSplit[8];
				Integer elementGroup = Integer.parseInt(elementInfoSplit[9]);
				String electronConfig = elementInfoSplit[10];
				String ionizationEnergy = elementInfoSplit[11];
				elementInfo element = new elementInfo(elementName, elementSymbol, elementNumber, elementMass, elementGroup, meltingPoint, boilingPoint, density, earthCrustPercent, discoveryYear, electronConfig, ionizationEnergy);
				elements.put(elementName, element);
			}
			System.out.println("FINSIHED: Elements setup");
			scanner.close();
		}
	}
}
