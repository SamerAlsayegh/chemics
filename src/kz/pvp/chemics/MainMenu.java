package kz.pvp.chemics;

import java.util.HashMap;
import java.util.Map.Entry;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import kz.pvp.chemics.chemistry.periodicTable;
import kz.pvp.chemics.enums.CalculationSystem;
import kz.pvp.chemics.enums.pageType;
import kz.pvp.chemics.utilities.Messages;
import android.support.v7.app.ActionBarActivity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

public class MainMenu extends ActionBarActivity{

	private static final String APP_VERSION = "1.2.0";
	public static MainMenu instance = null;
	static boolean inAnimation = false;
	static Integer openID = 0;
	Integer closeID = 0;
	static Integer deepLevel = 0;
	GoogleAnalytics tracker;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		instance = this;
		periodicTable.setUpElements();
		SubDetails.pT = pageType.About_Us;
		InputPage.formulaInfo.put(Messages.ConcentrationTitle, new CalculationSystem(Messages.ConcentrationDesc, Messages.ConcentrationFormula, new String[]{"Concentration (mol/L)-C", "Amount (mol)-N", "Volume (L)-V"}, new String[]{"N / V", "C * V", "N / C"}));
		InputPage.formulaInfo.put(Messages.NumberAtomsTitle, new CalculationSystem(Messages.NumberAtomsDesc, Messages.NumberAtomsFormula, new String[]{"Amount-n","Number of Atoms-N"}, new String[]{"N / ( 6.02 * 10 ^ 23 )", "n * 6.02 * 10 ^ 23"}));
		InputPage.formulaInfo.put(Messages.VeloInAirTitle, new CalculationSystem(Messages.VeloInAirDesc, Messages.VeloInAirFormula, new String[]{"Velocity of Sound in Air-V", "Temperature of Air-T"}, new String[]{"331.6 + 0.606 * T ","( V - 331.6 ) / 0.606"}));
		InputPage.formulaInfo.put(Messages.WorkFormulaTitle, new CalculationSystem(Messages.WorkFormulaDesc, Messages.WorkFormulaFormula, new String[]{"Work (Joules)-W", "Force (Newtons)-N", "Distance (Meters)-d", "Angle (θ)-θ"}, new String[]{"N * d * ( cos θ ) ", "W / ( d * ( cos θ ) )", "W / ( N * ( cos θ ) )", "cos⁻¹ ( W / ( N * d ) )"}));
		
		GoogleAnalytics.getInstance(this).setLocalDispatchPeriod(1);
		SharedPreferences sharedPref = instance.getPreferences(Context.MODE_PRIVATE);
		int chosenGrade = 0;
		chosenGrade = sharedPref.getInt("Grade", chosenGrade);
		if (chosenGrade == 0){
			toggleLayout(R.id.gradeChoosing, R.id.menuChoices);
		}
		else{
			//
		}
	}


	@Override
	public void onStart(){
		super.onStart();
		// Get tracker.
		Tracker t = getTracker(
				TrackerName.APP_TRACKER);
		t.setScreenName("Home");
		t.send(new HitBuilders.ScreenViewBuilder().build());
	}

	public void closeAboutus (View view){
		if (inAnimation){
			toggleLayout(R.id.menuChoices, R.id.aboutUs);
		}
	}

	@Override
	public void onBackPressed() {
		if (deepLevel == 1){
			if (inAnimation)
				toggleLayout(R.id.menuChoices, openID);
			else{
				super.onBackPressed();
				this.overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
			}
		}
		else if (deepLevel >= 2){
			setupMenus(SubDetails.pT);
			toggleLayout(R.id.list, openID);
		}
		else{
			super.onBackPressed();
			this.overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
		}
		Tracker t = getTracker(
				TrackerName.APP_TRACKER);

		// Set screen name.
		// Where path is a String representing the screen name.
		t.setScreenName("Noob");

		// Send a screen view.
		t.send(new HitBuilders.AppViewBuilder().build());
	}


	static void toggleLayout(final Integer openViewID, Integer closeViewID) {
		if (inAnimation){
			TranslateAnimation animText = new TranslateAnimation(0, 0, 0, 2000);
			animText.setDuration(500);
			final LinearLayout viewToClose = (LinearLayout) instance.findViewById(closeViewID);
			viewToClose.startAnimation(animText);
			inAnimation = false;
			final LinearLayout viewToOpen = (LinearLayout) instance.findViewById(openViewID);
			animText.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationEnd(Animation arg0) {
					viewToOpen.setVisibility(View.VISIBLE);
					TranslateAnimation anim = new TranslateAnimation(0, 0, 2000, 0);
					anim.setDuration(500);
					viewToOpen.startAnimation(anim);
					viewToClose.setVisibility(View.GONE);
					openID = openViewID;
				}

				@Override
				public void onAnimationRepeat(Animation arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationStart(Animation arg0) {
					// TODO Auto-generated method stub

				}
			});
		}
		else{
			final LinearLayout viewToClose = (LinearLayout) instance.findViewById(closeViewID);
			TranslateAnimation anim = new TranslateAnimation(0, 0, 0, 2000);
			anim.setDuration(500);
			anim.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationEnd(Animation arg0) {
					viewToClose.setVisibility(View.GONE);
					openID = openViewID;
					TranslateAnimation animText = new TranslateAnimation(0, 0, 2000, 0);
					animText.setDuration(500);
					final LinearLayout viewToOpen = (LinearLayout) instance.findViewById(openID);
					viewToOpen.startAnimation(animText);
					viewToOpen.setVisibility(View.VISIBLE);
					TranslateAnimation anim = new TranslateAnimation(0, 0, 2000, 0);
					anim.setDuration(500);
					inAnimation = true;
				}

				@Override
				public void onAnimationRepeat(Animation arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationStart(Animation arg0) {
					// TODO Auto-generated method stub

				}
			});
			viewToClose.startAnimation(anim);
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	public void togglePage(View view){
		int ID = view.getId();
		if (ID == R.id.Physics)
			openUpPage(pageType.Physics);
		else if (ID == R.id.Chemistry)
			openUpPage(pageType.Chemistry);
		else if (ID == R.id.Math)
			openUpPage(pageType.Math);
		else if (ID == R.id.AboutUs)
			toggleLayout(R.id.aboutUs, R.id.menuChoices);
		else if (ID == R.id.changeGrade)
			toggleLayout(R.id.gradeChoosing, R.id.menuChoices);
	}

	private void openUpPage(pageType pT) {
		// TODO Auto-generated method stub
		//Intent intent = new Intent(this, SubDetails.class);

		SubDetails.listOptions.clear();
		if (pT == pageType.Chemistry || pT == pageType.Physics || pT == pageType.Math){
			setupMenus(pT);
			SubDetails.pT = pT;
			toggleLayout(R.id.list, R.id.menuChoices);
		}
		this.overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
	}


	public static void setupMenus(pageType pT) {
		// TODO Auto-generated method stub
		deepLevel = 1;
		if (pT == pageType.Chemistry){
			SubDetails.listOptions.add("Periodic Table");// Done
			for (Entry<String, CalculationSystem> val : InputPage.formulaInfo.entrySet())
				SubDetails.listOptions.add(val.getKey());// Done
			SubDetails.listOptions.add("Dilution");// Done
			SubDetails.listOptions.add("Combined Gas law");// Done
			SubDetails.listOptions.add("Molar Mass - Combined");
		}
		else if (pT == pageType.Physics){
			SubDetails.listOptions.clear();
			SubDetails.listOptions.add(Messages.MotionEquationTitle);// Done
			SubDetails.listOptions.add(Messages.DopplerEffectTitle);// Done
			SubDetails.listOptions.add(Messages.ProjectileTitle);// Done
			SubDetails.listOptions.add(Messages.ThermalEnergyTitle);// Done
			SubDetails.listOptions.add(Messages.SnellsLawTitle);// Done
			for (Entry<String, CalculationSystem> val : InputPage.formulaInfo.entrySet())
				SubDetails.listOptions.add(val.getKey());// Done

			SubDetails.listOptions.add(Messages.ResistanceParallelTitle);// Done
			SubDetails.listOptions.add(Messages.ResistanceSeriesTitle);// Done
			SubDetails.listOptions.add(Messages.OhmsLawTitle);// Done

		}
		else if (pT == pageType.Math){
			SubDetails.listOptions.clear();
			SubDetails.listOptions.add("Quadratic Formula");// Done
			SubDetails.listOptions.add("Length between 2 points");// Done
			SubDetails.listOptions.add("Midpoint");// Done
			SubDetails.listOptions.add("Cosine Law");// Done
		}
		SubDetails.setupFragment();
	}

	/**
	 * This class makes the ad request and loads the ad.
	 */
	public static class AdFragment extends Fragment {

		public static AdView mAdView;

		public AdFragment() {
		}

		@Override
		public void onActivityCreated(Bundle bundle) {
			super.onActivityCreated(bundle);

			// Gets the ad view defined in layout/ad_fragment.xml with ad unit ID set in
			// values/strings.xml.
			mAdView = (AdView) getView().findViewById(R.id.adView);

			// Create an ad request. Check logcat output for the hashed device ID to
			// get test ads on a physical device. e.g.
			// "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
			AdRequest adRequest = new AdRequest.Builder()
			.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
			.build();

			// Start loading the ad in the background.
			mAdView.loadAd(adRequest);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_ad, container, false);
		}

		/** Called when leaving the activity */
		@Override
		public void onPause() {
			if (mAdView != null) {
				mAdView.pause();
			}
			super.onPause();
		}

		/** Called when returning to the activity */
		@Override
		public void onResume() {
			super.onResume();
			if (mAdView != null) {
				mAdView.resume();
			}
		}

		/** Called before the activity is destroyed */
		@Override
		public void onDestroy() {
			if (mAdView != null) {
				mAdView.destroy();
			}
			super.onDestroy();
		}
	}

	// The following line should be changed to include the correct property id.
	private static final String PROPERTY_ID = "UA-55230622-1";

	public static int GENERAL_TRACKER = 0;

	public enum TrackerName {
		APP_TRACKER, // Tracker used only in this app.
		GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
		ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
	}

	static HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

	synchronized static Tracker getTracker(TrackerName trackerId) {
		if (!mTrackers.containsKey(trackerId)) {

			GoogleAnalytics analytics = GoogleAnalytics.getInstance(instance);
			Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID)
					: (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
							: analytics.newTracker(R.xml.ecommerce_tracker);
					mTrackers.put(trackerId, t);

		}
		return mTrackers.get(trackerId);
	}
	public static void getAnswer(View view){
		InputPage.getAnswer();
	}
	public void showPopup(View anchorView) {
		InputPage.showPopup();
	}

	public void chosenGrade (View view){
		Integer gradeChosen = 0;

		switch (view.getId()) {
		case R.id.Grade9:
			gradeChosen = 9;
			break;
		case R.id.Grade10:
			gradeChosen = 10;
			break;
		case R.id.Grade11:
			gradeChosen = 11;
			break;
		case R.id.Grade12:
			gradeChosen = 12;
			break;
		}
		if (gradeChosen != 0){
			SharedPreferences sharedPref = instance.getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putInt("Grade", gradeChosen);
			editor.commit();
			toggleLayout(R.id.menuChoices, R.id.gradeChoosing);
		}
		System.out.println("View: " + view.getId());
	}
}
