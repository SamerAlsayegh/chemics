package kz.pvp.chemics.utilities;

public class Messages {

	public static final String SnellsLawTitle = "Snell's Law";
	public static final String SnellsLawDesc = "Enter the element symbols with correct capitals, and number after the element.";
	public static final String SnellsLawFormula = "n₁"  + "Sinθ₁ " + " = " + "n₂" + "Sinθ₂";
	
	public static final String MolarMassCombinedTitle = "Molar Mass - Combined";
	public static final String MolarMassCombinedDesc = "Enter the element symbols with correct capitals, and number after the element.";
	public static final String MolarMassCombinedFormula = "(Mm) + (Mm)...";
	
	public static final String CombinedGasLawTitle = "Combined Gas law";
	public static final String CombinedGasLawDesc = "All laws in one calculator.\n ATTENTION: Make sure both Pressure and Volume are in the same unit, and Temperature is in Kelvin.";
	public static final String CombinedGasLawFormula = "(P₁V₁)/T₁=(P₂V₂)/T₂";
	
	public static final String CosineLawTitle = "Cosine Law";	
	public static final String CosineLawDesc = "Fill in all known values.\nFor the angle, Side X must be opposite of the angle.";	
	public static final String CosineLawFormula = "c²=a²+b²-2abCos(θ)";
	
	public static final String MidpointTitle = "Midpoint";	
	public static final String MidpointDesc = "Enter the co-ordinates to find the midpoint between them.";
	public static final String MidpointFormula = "((X₁ + X₂)/2,(Y₁ + Y₂)/2)";
	
	public static final String LengthBetweenPointsTitle = "Length between 2 points";
	public static final String LengthBetweenPointsDesc = "Enter the Co-ordinates to find the distance between them.";
	public static final String LengthBetweenPointsHowto = "√((X₁-X₂)²+(Y₁-Y₂)²)";
	
	public static final String ThermalEnergyTitle = "Thermal Energy";
	public static final String ThermalEnergyDesc = "Fill in all the values you know. Keep the unknown, empty.";
	public static final String ThermalEnergyFormula = "Q=mcΔT";
	
	public static final String ProjectileTitle = "Projectile";
	public static final String ProjectileDesc = "Fill in all the known values. Keep the unknown empty.";
	public static final String ProjectileFormula = "Vy = V * Sin(θ°)\nVx = V * Cos(θ°)";
	
	public static final String QuadraticTitle = "Quadratic Formula";
	public static final String QuadraticDesc = "Fill in all the values from standard form parabola to find the roots(zeroes).";
	public static final String QuadraticFormula = "x = (-b ± √b² - 4ac)/2a";
	
	public static final String ConcentrationTitle = "Concentration";
	public static final String ConcentrationDesc = "Fill in all the known values. Keep the unknown empty.";
	public static final String ConcentrationFormula = "c = n/V";
	
	
	public static final String DopplerEffectTitle = "Doppler Effect";	
	public static final String DopplerEffectDesc = "Fill in all the known values. Keep the unknown empty.";
	public static final String DopplerEffectFormula = "Fd=(Vw/(Vw±Vs))Fs";
	
	public static final String DilutionTitle = "Dilution";
	public static final String DilutionDesc = "Fill in all the known values. Keep the unknown empty.";
	public static final String DilutionFormula = "V₁ * C₁ = V₂ * C₂";
	
	public static final String NumberAtomsTitle = "Number of Atoms/Moles";
	public static final String NumberAtomsDesc = "Fill in known value, leave unknown empty.";	
	public static final String NumberAtomsFormula = "N=Na*n";
	
	public static final String VeloInAirTitle = "Velocity of Sound in Air";
	public static final String VeloInAirDesc = "Input the known value.";
	public static final String VeloInAirFormula = "Temp = (Vs - 331.6) / 0.606";
	
	
	public static final String MotionEquationTitle = "Motion Equation";	
	public static final String MotionEquationDesc = "Input the known variables, input '#' for the variable you're trying to find.";	
	public static final String MotionEquationFormula = "Vf = Vi + at\n"
														+ "d = (Vf + Vi)/2t\n"
														+ "d = Vi * t + 1/2at²\n"
														+ "d = Vf * t - 1/2ad²\n"
														+ "Vf² = Vi² + 2ad";
	public static final String ResistanceParallelTitle = "Resistance In Parallel";
	public static final String ResistanceParallelDesc = "Keep all values in Omhs. Use add more fields for more entry space.";
	public static final String ResistanceParallelFormula = "1/R\u209A = 1/R₁ + 1/R₂ + 1/R₃...";

	public static final String ResistanceSeriesTitle = "Resistance In Series";
	public static final String ResistanceSeriesDesc = "Keep all values in Omhs. Use add more fields for more entry space.";
	public static final String ResistanceSeriesFormula = "R\u209B = R₁ + R₂ + R₃...";
	
	public static final String OhmsLawTitle = "Ohm's Law";
	public static final String OhmsLawDesc = "Keep all values in Volts, Amperes, and Ohms.";
	public static final String OhmsLawFormula = "V = IR";
	
	
	public static final String WorkFormulaTitle = "Work Done";
	public static final String WorkFormulaDesc = "Keep all values in Joules, Meters and Newtons.";
	public static final String WorkFormulaFormula = "W = F(cosθ)d";
	
	
	
	
	
	
}
