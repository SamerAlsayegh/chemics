package kz.pvp.chemics;

import java.util.ArrayList;

import kz.pvp.chemics.chemistry.periodicTable;
import kz.pvp.chemics.enums.elementInfo;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ActionBar.LayoutParams;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.AdapterView.OnItemClickListener;


public class PeriodicTable extends ListActivity {

	public static PeriodicTable instance = null;
	public static ArrayList<String> listOptions = new ArrayList<String>();

	public void showDialog(elementInfo details) {
		
	}

	public void showPopup(elementInfo details) {
		InfoPage.setupInfo(details);
		// our adapter instance
		ArrayAdapter adapter = new ArrayAdapter(this, R.layout.text_item, InfoPage.details);

		// create a new ListView, set the adapter and item click listener
		ListView listViewItems = new ListView(this);
		listViewItems.setDividerHeight(0);
		listViewItems.setAdapter(adapter);
		listViewItems.setBackgroundResource(R.drawable.button_round_border_green_light);
		// put the ListView in the pop up
		listViewItems.setPadding(20, 20, 20, 20);
		Dialog dialog = new Dialog(this, R.style.NewDialog);
		dialog.setContentView(listViewItems);
		dialog.setCanceledOnTouchOutside(true);
		dialog.show();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;
		int layout = R.layout.activity_list_page;
		if (listOptions.size() <= 0)
		for (elementInfo elementInfo : periodicTable.elements.values()){
			listOptions.add(elementInfo.getNumber() + " - " + elementInfo.getFullName());
		}



		setListAdapter(new ArrayAdapter<String>(this, layout, listOptions));

		final ListView listView = getListView();
		listView.setTextFilterEnabled(true);

		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String val = listOptions.get(position);

				if (periodicTable.elements.containsKey(val.split(" - ")[1])){
					elementInfo details = periodicTable.elements.get(val.split(" - ")[1]);
					//showDialog(details);
					showPopup(details);
				}
			}
		});

	}
}