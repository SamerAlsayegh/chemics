package kz.pvp.chemics;

import java.util.ArrayList;

import com.google.android.gms.analytics.GoogleAnalytics;

import kz.pvp.chemics.MainMenu.TrackerName;
import kz.pvp.chemics.chemistry.periodicTable;
import kz.pvp.chemics.enums.elementInfo;
import kz.pvp.chemics.enums.inputType;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class InfoPage extends ListActivity {
	static ArrayList<String> details = new ArrayList<String>();
	static InfoPage instance = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		instance = this;

		setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_element_info_page,details));

		final ListView listView = getListView();
		listView.setTextFilterEnabled(true);
		listView.setBackgroundResource(R.color.background_black_trans);
	}

	public static void setupInfo(elementInfo element) {
		details.clear();
		String extraInfo = " ";
		if (element.getGroup().equals(1))
		extraInfo += "(Alkali Metal)";
		else if (element.getGroup().equals(2))
			extraInfo += "(Alkaline Earth Metal)";
		else if (element.getGroup().equals(11))
			extraInfo += "(Coinage metals)";
		else if (element.getGroup().equals(15))
			extraInfo += "(Pnictogens)";
		else if (element.getGroup().equals(16))
			extraInfo += "(Chalcogens)";
		else if (element.getGroup().equals(17))
			extraInfo += "(Halogen)";
		else if (element.getGroup().equals(18))
			extraInfo += "(Noble Gas)";
		else if (element.getGroup() >= 3 && element.getGroup() <= 11)
			extraInfo += "(Transition elements)";
		else if (element.getGroup() >= 13 && element.getGroup() <= 18)
			extraInfo += "(Main group elements)";

		details.add("Number: " + element.getNumber());
		details.add("Name: " + element.getFullName());
		details.add("Symbol: " + element.getSymbol());
		details.add("Mass: " + element.getMass());
		details.add("Group: " + element.getGroup() + extraInfo);
		if (!element.getEarthCrust().equalsIgnoreCase("Unknown"))
		details.add("Melting Point: " + element.getMP() + " �C");
		if (!element.getEarthCrust().equalsIgnoreCase("Unknown"))
		details.add("Boiling Point: " + element.getBP() + " �C");
		if (!element.getEarthCrust().equalsIgnoreCase("Unknown"))
		details.add("Density: " + element.getDensity() + " g/cm3");
		if (!element.getEarthCrust().equalsIgnoreCase("Unknown"))
		details.add("Earth Crust: " + element.getEarthCrust() + "%");
		if (!element.getEarthCrust().equalsIgnoreCase("Unknown"))
		details.add("Discovery (Year): " + element.getDiscovery());
		if (!element.getEarthCrust().equalsIgnoreCase("Unknown"))
		details.add("Electron Config (Ionization Energy): " + element.getIonizationEnergy());

	}

}
