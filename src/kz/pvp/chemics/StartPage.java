package kz.pvp.chemics;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class StartPage extends Fragment {

	public static StartPage instance = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_start_page, container, false);
		instance = this;
		return view;
	}
	
	
	
}
