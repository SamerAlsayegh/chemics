package kz.pvp.chemics.physics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import kz.pvp.chemics.chemistry.periodicTable;
import kz.pvp.chemics.enums.elementInfo;

public class Calculations {
	//First Equation of motion
	public static String getMotionEq1(String Vi, String Vf, String A, String T){
		int nulls=0;
		double vi=0, vf=0, a=0, t=0;
		if (isDouble(Vi) && Vi.length() > 0)
			vi=Double.parseDouble(Vi);
		if (isDouble(Vf) && Vf.length() > 0)
			vf=Double.parseDouble(Vf);
		if (isDouble(A) && A.length() > 0)
			a=Double.parseDouble(A);
		if (isDouble(T) && T.length() > 0)
			t=Double.parseDouble(T);
		if (Vi.equalsIgnoreCase("#"))
			nulls++;
		if (Vf.equalsIgnoreCase("#"))
			nulls++;
		if (A.equalsIgnoreCase("#"))
			nulls++;
		if (T.equalsIgnoreCase("#"))
			nulls++;
		if (nulls == 1){
			if (Vi.equalsIgnoreCase("#")){
				vi=vf-a*t;
                String viGet="Vf = Vi + a x t/nVi = Vf - a x t/nVi = "+vf+" - "+a+" x "+t+"/nThe initial velocity is "+vi+".";
				return viGet;}
			if (Vf.equalsIgnoreCase("#")){
				vf=vi+a*t;             
				 String vfGet="Vf = Vi + aΔt/nVf = "+vi+" + "+a+" x "+t+"/nVf = "+vf+"/nThe final velocity is "+vf+".";
				return vfGet;}
			if (A.equalsIgnoreCase("#")){
				a=(vf-vi)/t;           
				String aGet="Vf = Vi + aΔt/naΔt = Vf - Vi/na=(Vf - Vi)/Δt/n a=("+vf+"-"+vi+")/"+t+"/na = "+a+"/nAcceleration is +"+a+".";
				return aGet;}
			if (T.equalsIgnoreCase("#")){
				t=(vf-vi)/a;           
				String tGet=t+":Time";
				return tGet;}
		}
		return null;}
	//Second equation of motion
	public static String getMotionEq2(String Vi, String Vf, String D, String T){
		int nulls=0;
		double vi=0, vf=0, d=0, t=0;
		if (isDouble(Vi) && Vi.length() > 0)
			vi=Double.parseDouble(Vi);
		if (isDouble(Vf) && Vf.length() > 0)
			vf=Double.parseDouble(Vf);
		if (isDouble(D) && D.length() > 0)
			d=Double.parseDouble(D);
		if (isDouble(T) && T.length() > 0)
			t=Double.parseDouble(T);
		if (Vi.equalsIgnoreCase("#"))
			nulls++;
		if (Vf.equalsIgnoreCase("#"))
			nulls++;
		if (D.equalsIgnoreCase("#"))
			nulls++;
		if (T.equalsIgnoreCase("#"))
			nulls++;
		if (nulls == 1){
			if (Vi.equalsIgnoreCase("#")){
				vi=(2*d/t)-vf;
				String viGet=vi+":Initial Velocity";
				return viGet;}
			if (Vf.equalsIgnoreCase("#")){
				vf=(2*d/t)-vi;         
				String vfGet=vf+":Final Velocity";
				return vfGet;}
			if (D.equalsIgnoreCase("#")){
				d=((vf+vi)/2)*t;               
				String dGet=d+":Distance";
				return dGet;}
			if (T.equalsIgnoreCase("#")){
				t=d/((vf+vi)/2);                       
				String tGet=t+":Time";
				return tGet;}
		}
		return null;
	}
	//Third equation of Motion
	public static String getMotionEq3(String Vi, String A, String D, String T){
		int nulls=0;
		double vi=0, a=0, d=0, t=0;
		if (isDouble(Vi) && Vi.length() > 0)
			vi=Double.parseDouble(Vi);
		if (isDouble(A) && A.length() > 0)
			a=Double.parseDouble(A);
		if (isDouble(D) && D.length() > 0)
			d=Double.parseDouble(D);
		if (isDouble(T) && T.length() > 0)
			t=Double.parseDouble(T);
		if (Vi.equalsIgnoreCase("#"))
			nulls++;
		if (A.equalsIgnoreCase("#"))
			nulls++;
		if (D.equalsIgnoreCase("#"))
			nulls++;
		if (T.equalsIgnoreCase("#"))
			nulls++;
		if (nulls == 1){
			if (Vi.equalsIgnoreCase("#")){
				vi=d-0.5*a*t;
				String viGet=vi+":Initial Velocity";
				return viGet;}
			if (A.equalsIgnoreCase("#")){
				a=(2*(d-vi*t))/(t*t);
				String aGet=a+":Acceleration";
				return aGet;}
			if (D.equalsIgnoreCase("#")){
				d=(vi*t)+0.5*a*t*t;
				String dGet=d+":Distance";
				return dGet;}
			if (T.equalsIgnoreCase("#")){
				t=(d-vi)/(0.5*a);
				String tGet=t+":Time";
				return tGet;}
		}
		return null;}  

	//Fourth Equation of motion
	public static String getMotionEq4(String Vf, String A, String D, String T){
		int nulls=0;
		double vf=0, a=0, d=0, t=0;
		if (isDouble(Vf) && Vf.length() > 0)
			vf=Double.parseDouble(Vf);
		if (isDouble(A) && A.length() > 0)
			a=Double.parseDouble(A);
		if (isDouble(D) && D.length() > 0)
			d=Double.parseDouble(D);
		if (isDouble(T) && T.length() > 0)
			t=Double.parseDouble(T);
		if (Vf.equalsIgnoreCase("#"))
			nulls++;
		if (A.equalsIgnoreCase("#"))
			nulls++;
		if (D.equalsIgnoreCase("#"))
			nulls++;
		if (T.equalsIgnoreCase("#"))
			nulls++;
		if (nulls == 1){
			if (Vf.equalsIgnoreCase("#")){
				vf=d+0.5*a*t;
				String vfGet=vf+":Final Velocity";
				return vfGet;}
			if (A.equalsIgnoreCase("#")){
				a=(d+vf)/(0.5*t);              
				String aGet=a+":Acceleration";
				return aGet;}
			if (D.equalsIgnoreCase("#")){
				d=vf*t-0.5*a*t*t;              
				String dGet=d+":Distance";
				return dGet;}
			if (T.equalsIgnoreCase("#")){
				t=(d+vf)/(0.5*a);                      
				String tGet=t+":Time";
				return tGet;}
		}
		return null;}   /*
    public static String getCoords(String x1, String x2, String y1, String y2){
            int nulls = 0;
            double x=0, y=0, ;=0, vi=0;
            if (x1.length() >0 ){
                    vf=Double.parseDouble(Vf);}
            if (x2.length() >0 ){
                    a=Double.parseDouble(A);}
            if (y1.length() >0 ){
                    d=Double.parseDouble(D);}
            if (y2.length() >0 ){
                    vi=Double.parseDouble(Vi);}
            if (x1.equalsIgnoreCase("#")){
                    nulls++;}
            if (x2.equalsIgnoreCase("#")){
                    nulls++;}
            if (y1.equalsIgnoreCase("#")){
                    nulls++;}
            if (y2.equalsIgnoreCase("#")){
                    nulls++;}
            if (nulls == 0){
                    double x=x2-x1;
                    double y

                    }
    }*/
	//fifth

	public static String getMotionEq5(String Vf, String A, String D, String Vi){
		int nulls=0;
		double vf=0, a=0, d=0, vi=0;
		if (isDouble(Vf) && Vf.length() > 0)
			vf=Double.parseDouble(Vf);
		if (isDouble(A) && A.length() > 0)
			a=Double.parseDouble(A);
		if (isDouble(D) && D.length() > 0)
			d=Double.parseDouble(D);
		if (isDouble(Vi) && Vi.length() > 0)
			vi=Double.parseDouble(Vi);
		if (Vf.equalsIgnoreCase("#"))
			nulls++;
		if (A.equalsIgnoreCase("#"))
			nulls++;
		if (D.equalsIgnoreCase("#"))
			nulls++;
		if (Vi.equalsIgnoreCase("#"))
			nulls++;
		if (nulls == 1){
			if (Vf.equalsIgnoreCase("#")){
				vf=Math.sqrt(vi*vi+2*a*d);
				String vfGet=vf+":Final Velocity";
				return vfGet;}
			if (A.equalsIgnoreCase("#")){
				a=(vf*vf-vi*vi)/(2*d);         
				String aGet=a+":Acceleration";
				return aGet;}
			if (D.equalsIgnoreCase("#")){
				d=(vf*vf-vi*vi)/(2*a);         
				String dGet=d+":Distance";
				return dGet;}
			if (Vi.equalsIgnoreCase("#")){
				vi=Math.sqrt(vf*vf-2*a*d);             
				String viGet=vi+":Initial Velocity";
				return viGet;}
		}
		return null;
	}

	//Velocity of Sound
	public static String vSoundInAir(String Vs, String Temp){
		int nulls=0;
		double vs=0, temp=0;
		if (isDouble(Vs) && Vs.length() > 0)
			vs=Double.parseDouble(Vs);
		if (isDouble(Temp) && Temp.length() > 0)
			temp=Double.parseDouble(Temp);
		if(Vs.isEmpty())
			nulls++;
		if(Temp.isEmpty())
			nulls++;
		if (nulls==1){
			if (Vs.isEmpty()){
				vs=33.16+0.606*temp;
				String getVs=vs+":Velocity of Sound";
				return getVs;
			}
			else if (Temp.isEmpty()){
				temp=(vs-331.6)/0.606;
				String tempGet=temp+":Temperature of Air";
				return tempGet;
			}
		}
		return null;   
	}

	public static String numberOfAtoms(String moles, String N){
		int nulls=0;
		double n=0, Na=0;              
		if (isDouble(moles) && moles.length() > 0)
			n=Double.parseDouble(moles);
		if (isDouble(N) && N.length() > 0)
			Na=Double.parseDouble(N);
		if (moles.isEmpty())
			nulls++;
		if (N.isEmpty())
			nulls++;
		if (nulls == 1){
			if (moles.isEmpty()){

				n=Na/(6.02*Math.pow(10, 23));
				String nGet=n+":Moles";                        
				return nGet;
			}
			else if (N.isEmpty()){
				Na=n*(6.02*Math.pow(10, 23));
				String NaGet=Na+":Number of Molecules/Atoms";
				return NaGet;
			}
		}return null;  
	}
	public static String Dilution(String C1, String C2, String V1, String V2){
		int nulls=0;
		double c1=0, c2=0, v1=0, v2=0;
		if (isDouble(C1) && C1.length() > 0)
			c1=Double.parseDouble(C1);
		if (isDouble(C2) && C2.length() > 0)
			c2=Double.parseDouble(C2);
		if (isDouble(V1) && V1.length() > 0)
			v1=Double.parseDouble(V1);
		if (isDouble(V2) && V2.length() > 0)
			v2=Double.parseDouble(V2);
		if (C1.isEmpty())
			nulls++;
		if (C2.isEmpty())
			nulls++;
		if (V1.isEmpty())
			nulls++;
		if (V2.isEmpty())
			nulls++;
		if (nulls == 1){
			if (V1.isEmpty()){
				v1=(c2*v2)/c1;
				String v1Get=v1+":Volume 1";
				return v1Get;}
			if (V2.isEmpty()){
				v2=(c1*v1)/c2;          
				String v2Get=v2+":Volume 2";
				return v2Get;}
			if (C1.isEmpty()){
				c1=(c2*v2)/v1;           
				String c1Get=c1+":Concentration 1";
				return c1Get;}
			if (C2.isEmpty()){
				c2=(c1*v1)/v2;           
				String c2Get=c2+":Concentration 2";
				return c2Get;}
		}
		return null;
	}
	public static String quadFormula(String A, String B, String C){
		double a = 0, b = 0, c = 0, x1, x2, x3, x4;
		int nulls = 0;
		if (isDouble(A) && A.length() > 0)
			a=Double.parseDouble(A);
		if (isDouble(B) && B.length() > 0)
			b=Double.parseDouble(B);
		if (isDouble(C) && C.length() > 0)
			c=Double.parseDouble(C);
		String result;

		if (A.isEmpty())
			nulls++;
		if (B.isEmpty())
			nulls++;
		if (C.isEmpty())
			nulls++;

		if (nulls == 0){
			x1 = b*b;
			x1 = x1 - 4*a*c;
			if(x1 < 0){
				result="No Zeroes";
				return result;}
			else if(x1 == 0){
				x4 = (-1*b/(2*a));
				result ="One Zero:"+x4;
				return result;}
			else if(x1 > 0){
				x1 = Math.sqrt(x1);
				x2 = (-1*b - x1)/(2*a);
				x3 = (-1*b + x1)/(2*a);
				result ="Two Zeroes:"+x2+":"+x3;
				return result;
			}
		}
		return null;
	}
	public static boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
		} catch(NumberFormatException e) {
			return false;
		}
		return true;
	}
	public static String doppler(String Fd, String Fs, String Vw, String Vs, boolean Approaching){
		int nulls=0;
		double fd=0, fs=0, vw=0, vs=0;
		if (isDouble(Fd) && Fd.length() > 0)
			fd=Double.parseDouble(Fd);
		if (isDouble(Fs) && Fs.length() > 0)
			fs=Double.parseDouble(Fs);
		if (isDouble(Vw) && Vw.length() > 0)
			vw=Double.parseDouble(Vw);
		if (isDouble(Vs) && Vs.length() > 0)
			vs=Double.parseDouble(Vs);
		if (Fd.isEmpty())
			nulls++;
		if (Fs.isEmpty())
			nulls++;
		if (Vw.isEmpty())
			nulls++;
		if (Vs.isEmpty())
			nulls++;
		if (nulls == 1){
			if (Vw.isEmpty()){
				vw=(fd*vs)/(fd-fs);
				String vwGet=vw+":Velocity of sound in air";
				return vwGet;
			}
			if (Approaching){
				if (Fd.isEmpty()){
					fd=(vw/(vw-vs))*fs;
					String fdGet=fd+":Approaching Doppler Frequency";
					return fdGet;
				}
				if (Fs.isEmpty()){
					fs=fd/(vw/(vw-vs));
					String fsGet=fs+":Approaching Source Frequency";
					return fsGet;
				}

				if (Vs.isEmpty()){
					vs=(vw*(fd-fs))/fd;
					String vsGet=vs+":Approaching Source Velocity";
					return vsGet;
				}
			}
			else if (Approaching == false){
				if (Fd.isEmpty()){
					fd=(vw/(vw+vs))*fs;
					String fdGet=fd+":Receding Doppler Frequency";
					return fdGet;
				}
				if (Fs.isEmpty()){
					fs=fd/(vw/(vw+vs));
					String fsGet=fs+":Receding Source Frequency";
					return fsGet;
				}
				if (Vs.isEmpty()){
					vs=(vw*(fd-fs))/(-1*fd);
					String vsGet=vs+":Receding Source Velocity";
					return vsGet;
				}
			}  
		}
		return null;
	}
	public static String Concentration(String c, String n, String v) {
		int nulls=0;
		double C=0, N=0, V=0;
		if (isDouble(c) && c.length() > 0)
			C=Double.parseDouble(c);
		if (isDouble(n) && n.length() > 0)
			N=Double.parseDouble(n);
		if (isDouble(v) && v.length() > 0)
			V=Double.parseDouble(v);
		if (c.isEmpty())
			nulls++;
		if (n.isEmpty())
			nulls++;
		if (v.isEmpty())
			nulls++;
		if (nulls == 1){
			String ans = null;
			if (c.isEmpty()){
				C=N/V;
				ans=C+":Concentration";
			}
			else if (n.isEmpty()){
				N = C * V;
				ans=N+":Moles";
			}
			else if (v.isEmpty()){
				V = N/C;
				ans=C+":Volume";
			}

			return ans;
		}
		return null;
	}

	public static String ThermalEnergy(String Q, String m, String c, String tD) {
		int nulls=0;
		double q=0, M=0, C=0,TD = 0;
		if (isDouble(Q) && Q.length() > 0)
			q=Double.parseDouble(Q);
		if (isDouble(m) && m.length() > 0)
			M=Double.parseDouble(m);
		if (isDouble(c) && c.length() > 0)
			C=Double.parseDouble(c);
		if (isDouble(tD) && tD.length() > 0)
			TD=Double.parseDouble(tD);
		if (Q.isEmpty())
			nulls++;
		if (m.isEmpty())
			nulls++;
		if (c.isEmpty())
			nulls++;
		if (tD.isEmpty())
			nulls++;

		if (nulls == 1){
			double calc = 0.0;
			String ans = null;
			if (Q.isEmpty()){
				calc = M * C * TD;
				ans = calc  + ":Thermal Energy";
			}
			else if (m.isEmpty()){
				calc = q/(C * TD);
				ans = calc  + ":Mass";
			}
			else if (c.isEmpty()){
				calc = q/(M * TD);
				ans = calc  + ":Thermal Capacity";
			}
			else if (tD.isEmpty()){
				calc = q /(M * C);
				ans = calc  + ":Temperature Difference";
			}
			return ans;
		}
		return null;
	}
	/////////////////
	public static String Projectile(String theta, String v, String vZ, boolean y) {
		int nulls=0;
		double Theta=0, V=0, Vz=0;
		if (isDouble(theta) && theta.length() > 0)
			Theta=Double.parseDouble(theta);
		if (isDouble(v) && v.length() > 0)
			V=Double.parseDouble(v);
		if (isDouble(vZ) && vZ.length() > 0)
			Vz=Double.parseDouble(vZ);
		if (theta.isEmpty())
			nulls++;
		if (v.isEmpty())
			nulls++;
		if (vZ.isEmpty())
			nulls++;
		if (nulls == 1){
			double calc = 0.0;
			if (y)
				calc = Math.sin(Math.toRadians(Theta));
			else
				calc = Math.cos(Math.toRadians(Theta));
			String ans = null;
			if (theta.isEmpty()){
				if (y)
					Theta = Math.asin(Vz / V) * 180/Math.PI;
				else
					Theta = Math.acos(Vz / V) * 180/Math.PI;

				ans=Theta+":Theta";
				return ans;
			}
			else if (vZ.isEmpty()){
				Vz = V * calc;
				if (y)
					ans = Vz + ":Y Velocity Component";
				else
					ans = Vz + ":X Velocity Component";
				return ans;

			}
			else if (v.isEmpty()){
				V = Vz / calc; 
				ans=V+":Velocity";
				return ans;
			}
			else
				return null;
		}
		return null;
	}

	public double cosLaw(double a, double b, double c){
		double anglea;
		anglea = Math.pow(a,2) - Math.pow(b, 2) - Math.pow(c, 2);
		anglea = anglea/(-2*b*c);
		anglea = Math.acos(anglea);
		anglea = anglea*180/Math.PI;
		anglea = Math.round(anglea);

		return anglea;
	}
	public static String getMidpoint(String x1, String x2, String y1, String y2){
		int nulls=0;
		double Y1 = 0, Y2=0, X1=0, X2=0;
		if (isDouble(x1) && x1.length() > 0)
			X1=Double.parseDouble(x1);
		if (isDouble(x2) && x2.length() > 0)
			X2=Double.parseDouble(x2);
		if (isDouble(y1) && y1.length() > 0)
			Y1=Double.parseDouble(y1);
		if (isDouble(y2) && y2.length() > 0)
			Y2=Double.parseDouble(y2);

		if (x1.isEmpty() || x2.isEmpty() || y1.isEmpty() || y2.isEmpty())
			nulls++;
		if (nulls == 0){
			cordSet cord1 = new cordSet(X1, Y1);
			cordSet cord2 = new cordSet(X2, Y2);
			mathGridInfo grid = new mathGridInfo(cord1, cord2);
			return grid.getMidpoint();
		}
		return null;
	}
	public static String getLength(String x1, String x2, String y1, String y2){
		int nulls=0;
		double Y1 = 0, Y2=0, X1=0, X2=0;
		if (isDouble(x1) && x1.length() > 0)
			X1=Double.parseDouble(x1);
		if (isDouble(x2) && x2.length() > 0)
			X2=Double.parseDouble(x2);
		if (isDouble(y1) && y1.length() > 0)
			Y1=Double.parseDouble(y1);
		if (isDouble(y2) && y2.length() > 0)
			Y2=Double.parseDouble(y2);

		if (x1.isEmpty() || x2.isEmpty() || y1.isEmpty() || y2.isEmpty())
			nulls++;
		if (nulls == 0){
			cordSet cord1 = new cordSet(X1, Y1);
			cordSet cord2 = new cordSet(X2, Y2);
			mathGridInfo grid = new mathGridInfo(cord1, cord2);
			return grid.getLength();
		}
		return null;
	}
	public int checkTriangle(double sidea, double sideb, double sidec){
		double angleA = cosLaw(sidea, sideb, sidec);
		double angleB = cosLaw(sideb, sidea, sidec);
		double angleC = cosLaw(sidec, sidea, sideb);
		if((angleA+angleB+angleC) == 180){
			if ((angleA==0)||(angleB==0)||(angleC==0)) {
				return 6;
			}
			else if ((angleA==angleB) || (angleB==angleC) || (angleC==angleA)) {
				return 2;
			}
			else if (angleA==angleB && angleA==angleC){
				return 3;
			}
			else if ((angleA== 90) ||(angleB== 90) ||(angleC== 90)){
				return 4;
			}
			else if ((angleA > 90)||(angleB > 90)||(angleC > 90)){
				return 5;
			}

			else
				return 1;

		}

		else return 0;
	}

	public static class cordSet {
		double x, y;
		public cordSet (double xCord, double yCord) {
			this.x = xCord;
			this.y = yCord;
		}
		public double getX() {
			return x;
		}
		public double getY() {
			return y;
		}
	}

	public static class mathGridInfo {
		cordSet p1, p2;
		public mathGridInfo (cordSet punkt1, cordSet punkt2) {
			this.p1 = punkt1;
			this.p2 = punkt2;
		}
		public cordSet midPoint() {
			return new cordSet ((p1.getX()+p2.getX())/2, (p1.getY()+p2.getY())/2);
		}
		public double length() {
			return Math.sqrt(
					(p1.getX() - p2.getX()) *  (p1.getX() - p2.getX()) +
					(p1.getY() - p2.getY()) *  (p1.getY() - p2.getY())
					);
		}
		public String getLength(){
			mathGridInfo s = new mathGridInfo (p1, p2);

			double as = s.length();
			return as + ":Length between points";
		}
		public String getMidpoint(){
			mathGridInfo s = new mathGridInfo (p1,p2);
			cordSet mp = s.midPoint();
			return "(" + mp.getX() + "," + mp.getY() + "):Midpoint";
		}
	}

	public static String cosLaw(String A, String B, String C, String ANGLE){
		Double a, b, c, anglea;
		HashMap<String, Double> values = getValues(A,B,C,ANGLE);
		a = values.get(A);
		b = values.get(B);
		c = values.get(C);
		anglea = values.get(ANGLE);
		if (getNulls(values) == 1){

			if (A.isEmpty()){
				a=(Math.pow(b, 2)+Math.pow(c, 2))-(2*b*c*Math.cos(anglea));
				String aget=a+":Side X";
				return aget;

			}
			if (B.isEmpty()){
				b=(Math.pow(a, 2)+Math.pow(c, 2))-(2*a*c*Math.cos(anglea));
				String zget=c+":Side Y";
				return zget;
			}
			if (C.isEmpty()){
				c=(Math.pow(b, 2)+Math.pow(c, 2))-(2*b*c*Math.cos(anglea));
				String cget=c+":Side Z";
				return cget;
			}
			if (ANGLE.isEmpty()){
				anglea = Math.pow(a,2) - Math.pow(b, 2) - Math.pow(c, 2);
				anglea = anglea/(-2*b*c);
				anglea = Math.acos(anglea);
				anglea = anglea*180/Math.PI;
				anglea = (double) Math.round(anglea);
				String angleaGet=anglea+":Angle of side X";
				return angleaGet;
			}


		}              

		return null;
	}

	public static String molarMass(String elements){
		String[] res = elements.split("(?=\\p{Upper})");
		double totalMass = 0.0;

		for (String t : res) {
			if (!t.isEmpty()){
				if(t.matches(".*\\d.*")){
					// contains a number
					String[] splitData = t.split("[^A-Z0-9]+|(?<=[A-Z])(?=[0-9])|(?<=[0-9])(?=[A-Z])");
					String elementName = null;
					for (String data : splitData){
						if (!data.isEmpty()){
							if(isDouble(data)){
								// Number
								Integer amount = Integer.parseInt(data);
								totalMass += periodicTable.elements.get(elementName).getMass() * amount;
							}
							else{
								// Not a number.
								for (Entry<String, elementInfo> info : periodicTable.elements.entrySet()){
									if (info.getValue().getSymbol().equalsIgnoreCase(data) || info.getValue().getFullName().equalsIgnoreCase(data)){
										elementName = info.getKey();
										System.out.println("Found element on PT");
										break;
									}
								}
								if (elementName == null){
									return null;

								}
							}
						}
					}
				} else{
					// does not contain a number
					String elementName = null;
					for (Entry<String, elementInfo> info : periodicTable.elements.entrySet()){
						if (info.getValue().getSymbol().equalsIgnoreCase(t) || info.getValue().getFullName().equalsIgnoreCase(t)){
							elementName = info.getKey();
							totalMass += info.getValue().getMass();
							break;
						}
					}
					if (elementName == null){
						return null;
					}
				}
			}
		}
		return totalMass + ":Molar Mass";
	}


	public static String combLaw(String P1, String V1, String T1, String P2, String V2, String T2) {
		Double p1,v1,t1,p2,v2,t2;
		HashMap<String, Double> values = getValues(P1,V1,T1,P2,V2,T2);
		p1 = values.get(P1);
		v1 = values.get(V1);
		t1 = values.get(T1);
		p2 = values.get(P2);
		v2 = values.get(V2);
		t2 = values.get(T2);



		if(t1 == null && t2 == null){
			if(p1 != null&&p2 != null){
				if(v1 != null){
					v2=(p1*v1)/(p2);
					String v2Get=v2+":Volume 2";
					return v2Get;
				} else {
					v1=(p2*v2)/(p1);
					String v1Get=v1+":Volume 1";
					return v1Get;
				}
			} else if(v1 != null&&v2 != null){
				if(p1 != null){
					p2=(p1*v1)/v2;
					String p2Get=p2+":Pressure 2";
					return p2Get;
				} else {
					p1=(p2*v2)/v1;
					String p1Get=p1+":Pressure 1";
					return p1Get;
				}
			}
		} else if(p1 == null&&p2 == null){
			if(t1 != null&&t2 != null){
				if(v1 != null){
					v2=(v1*t2)/(t1);
					String v2Get=v2+":Volume 2";
					return v2Get;
				}
				else {
					v1=(v2*t1)/(t2);
					String v1Get=v1+":Volume 1";
					return v1Get;
				}
			}else if(v1 != null&&v2 != null){
				if(t1 != null){
					t2=(t1*v2)/v1;
					String t2Get=t2+":Temperature 2";
					return t2Get;
				} else {
					t1=(t2*v1)/v2;
					String t1Get=t1+":Temperature 1";
					return t1Get;
				}
			}
		} else if(v1 != null==false&&v2 != null==false){
			if(t1 != null&&t2 != null){
				if(p1 != null){
					p2=(p1*t2)/(t1);
					String p2Get=p2+":Pressure 2";
					return p2Get;
				}
				else {
					p1=(p2*t1)/(t2);
					String p1Get=p1+":Pressure 1";
					return p1Get;
				}
			}else if(p1 != null && p2 != null){
				if(t1 != null){
					t2=(t1*p2)/p1;
					String t2Get=t2+":Temperature 2";
					return t2Get;
				} else {
					t1=(t2*p1)/p2;
					String t1Get=t1+":Temperature 1";
					return t1Get;
				}
			}
		} else if(p1 != null&&v1 != null&&t1 != null){
			if(p2 != null){
				if(v2 != null){
					t2=(t1*p2*v2)/(p1*v1);
					String t2Get=t2+":Temperature 2";
					return t2Get;
				} else{
					v2=(t2*p1*v1)/(t1*p2);
					String v2Get=v2+":Volume 2";
					return v2Get;
				}
			} else if(v2 != null&&t2 != null){
				p2=(t2*p1*v1)/(v2*t1);
				String p2Get=p2+":Pressure 2";
				return p2Get;
			}

		} else if(p2 != null && v2 != null && t2 != null){
			if(p1 != null){
				if(v1 != null){
					t1=(t2*p1*v1)/(p2*v2);
					String t1Get=t2+":Temperature 1";
					return t1Get;
				} else{
					v1=(t1*p2*v2)/(t2*p1);
					String v1Get=v1+":Volume 1";
					return v1Get;
				}
			} else if(v1 != null && t1 != null){
				p1=(t1*p2*v2)/(v1*t2);
				String p1Get=p1+":Pressure 1";
				return p1Get;
			}
		}
		return null;
	}



	public static String snellsLaw(String n1, String sin1, String n2, String sin2) {
		Double Sin1, Sin2, N1, N2;

		HashMap<String, Double> values = getValues(n1,n2,sin1,sin2);
		N1 = values.get(n1);
		N2 = values.get(n2);
		Sin1 = values.get(sin1);
		Sin2 = values.get(sin2);

		if (getNulls(values) == 1){
			if (N1 == null)
				return (N2 * Math.sin(Math.toRadians(Sin2)))/ Math.sin(Math.toRadians(Sin1)) + ":N1";
			else if (N2 == null)
				return (N1 * Math.sin(Math.toRadians(Sin1)))/ Math.sin(Math.toRadians(Sin2)) + ":N2";
			else if (Sin1 == null)
				return (Math.asin((N2 * Math.sin(Math.toRadians(Sin2)))/ N1)* 180/Math.PI)  + ":θ1";
			else if (Sin2 == null)
				return (Math.asin((N1 * Math.sin(Math.toRadians(Sin1)))/ N2)* 180/Math.PI) + ":θ2";
		}
		return null;
	}

	public static String resistanceParallel(ArrayList<String> listEntries) {
		String[] listArray = new String[listEntries.size()];
		listArray = listEntries.toArray(listArray);
		System.out.println(listArray.length);
		HashMap<String, Double> values = getValues(listArray);
		Double rP = null;
		Double rT = null;
		if (getNulls(values) == 0){
			for (Double vals : values.values()){
				if (rP == null)
					rP = 0.0;
				rP += 1/vals;

			}
			rT=1/rP;
		}
		if (rT == null)
			return null;
		else
			return rT + ":Resistance in Parallel";
	}

	public static String resistanceSeries(ArrayList<String> listEntries) {
		String[] listArray = new String[listEntries.size()];
		listArray = listEntries.toArray(listArray);
		System.out.println(listArray.length);
		HashMap<String, Double> values = getValues(listArray);
		Double rP = null;
		if (getNulls(values) == 0){
			for (Double vals : values.values()){
				if (rP == null)
					rP = 0.0;
				rP += vals;
			}
		}
		return rP + ":Resistance in Series";
	}

	public static String ohmsLaw(String v, String i, String r) {
		Double V, I, R;
		HashMap<String, Double> values = getValues(v,i,r);
		V = values.get(v);
		I = values.get(i);
		R = values.get(r);

		if (getNulls(values) == 1){
			if (V == null)
				return (I * R) + ":Voltage (Volts)";
			else if (I == null)
				return (V/R) + ":Current (Amperes)";
			else if (R == null)
				return (V/I) + ":Resistance (Ohms)";
		}
		return null;
	}


	public static HashMap<String, Double> getValues(String... valuesNames){
		HashMap<String, Double> valuesList = new HashMap<String, Double>();

		for (String valueUnparsed : valuesNames){
			if (isDouble(valueUnparsed) && valueUnparsed.length() > 0)
				valuesList.put(valueUnparsed, Double.parseDouble(valueUnparsed));
			else
				valuesList.put(valueUnparsed, null);
		}
		return valuesList;
	}

	public static Integer getNulls(HashMap<String, Double> list){
		int nulls = 0;
		for (Entry<String, Double> entry : list.entrySet())
			if (entry.getValue()==null)
				nulls++;
		return nulls;
	}

}
